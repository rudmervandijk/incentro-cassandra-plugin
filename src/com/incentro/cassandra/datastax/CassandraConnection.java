/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.security.cert.CertificateException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.ProtocolOptions;
import com.datastax.driver.core.SSLOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.AuthenticationException;
import com.datastax.driver.core.exceptions.DriverException;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.datastax.driver.core.exceptions.UnauthorizedException;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import com.google.common.cache.LoadingCache;

/**
 * Holds a {@link com.datastax.driver.core.Session} shared among multiple clients.
 *
 * Each client gets a reference to this object by calling
 * {@link Utils#connect(String, String, String, String, String, boolean, String, String)}.
 *
 * Once the client is done interacting with Cassandra, it MUST NOT close
 * the session but instead MUST call {@link #release()}. The session
 * will close automatically when the last client has released.
 */
public class CassandraConnection {
	private static final Logger logger = LoggerFactory.getLogger(CassandraConnection.class);

	private final Map<String, String> config;

	// the cache where this object comes from
	private final LoadingCache<Map<String, String>, CassandraConnection> owningCache;

	// the number of clients that have a reference to this object
	private final AtomicInteger references = new AtomicInteger();

	private final Session session;

	CassandraConnection(Map<String, String> config, LoadingCache<Map<String, String>, CassandraConnection> owningCache)
			throws CassandraConnectionException {
		this.config = config;
		this.owningCache = owningCache;
		this.session = createSession(this.config);
	}

	public Session getSession() {
		return session;
	}

	/** This must be called by a client once it's done using the session. */
	public void release() {
		int newRef;
		while (true) {
			int ref = references.get();
			// We set to -1 after the last release, to distinguish it from the initial state
			newRef = (ref == 1) ? -1 : ref - 1;
			if (references.compareAndSet(ref, newRef)) break;
		}
		if (newRef == -1) {
			logger.debug("Released last reference to {}, closing Session", config);
			dispose();
		}
		else {
			logger.debug("Released reference to {}, new count = {}", config, newRef);
		}
	}

	/**
	 * Called internally when a client tries to acquire a reference to this object.
	 *
	 * @return whether the reference was acquired successfully
	 */
	boolean acquire() {
		while (true) {
			int ref = references.get();
			if (ref < 0) {
				// We raced with the release of the last reference, the caller will need to create a new session
				logger.debug("Failed to acquire reference to {}", config);
				return false;
			}
			if (references.compareAndSet(ref, ref + 1)) {
				logger.debug("Acquired reference to {}, new count = {}", config, ref + 1);
				return true;
			}
		}
	}

	private void dispose() {
		// No one else has a reference to the parent Cluster, and only one Session was created from it:
		session.getCluster().close();
		owningCache.invalidate(config);
	}

	// What follows is the standard code to initialise the Cluster and Session and log various exceptions with helpful diagnostic
	// information
	private Session createSession(Map<String, String> config) throws CassandraConnectionException {

		String nodes = config.get("nodes");
		String port_s = config.get("port");
		String username = config.get("username");
		String password = config.get("password");
		String keyspace = config.get("keyspace");
		boolean withSSL = Boolean.valueOf(config.get("withSSL"));
		String truststoreFilePath = config.get("truststoreFilePath");
		String truststorePass = config.get("truststorePass");
		ConnectionCompression compression = ConnectionCompression.fromString(config.get("compression"));

		Collection<InetSocketAddress> addresses = new LinkedList<InetSocketAddress>();
		if (nodes != null && nodes.trim().length() > 0) {
			// TODO add separator to call as parameter
			for (final String parts : nodes.split("\\|")) {
				final String host = parts.trim();
				final int port = Integer.parseInt(port_s);
				addresses.add(new InetSocketAddress(host, port));
			}
		}
		else {
			throw new CassandraConnectionException("No host(s) for cassandra provided");
		}

		Cluster.Builder builder = Cluster.builder().addContactPointsWithPorts(addresses);

		if (username != null && password != null && username.trim().length() + password.trim().length() > 0) {
			builder = builder.withCredentials(username, password);
		}
		if (withSSL && (truststoreFilePath == null || truststoreFilePath.isEmpty())) {
			builder = builder.withSSL();
		}
		else if (withSSL) {
			builder = builder.withSSL(getSSLOptions(truststoreFilePath, truststorePass));
		}

		// set compression
		switch (compression) {
		case SNAPPY:
			builder.withCompression(ProtocolOptions.Compression.SNAPPY);
			break;
		case NONE:
			builder.withCompression(ProtocolOptions.Compression.NONE);
			break;
		case LZ4:
			builder.withCompression(ProtocolOptions.Compression.LZ4);
			break;
		default:
			logger.warn("Compression " + compression.toString() + " is not implemented, ignored");
		}

		// default load balancing
		builder.withLoadBalancingPolicy(new TokenAwarePolicy(DCAwareRoundRobinPolicy.builder().build()));

		builder.withPoolingOptions(new PoolingOptions()
				.setCoreConnectionsPerHost(HostDistance.REMOTE, 1)
				.setCoreConnectionsPerHost(HostDistance.LOCAL, 1)
				.setMaxConnectionsPerHost(HostDistance.REMOTE, 1)
				.setMaxConnectionsPerHost(HostDistance.LOCAL, 1)
				.setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
				.setMaxRequestsPerConnection(HostDistance.REMOTE, 32768)
				.setNewConnectionThreshold(HostDistance.REMOTE, 128));

		try {
			Cluster cluster = builder.build();
			Session result = cluster.connect(keyspace);
			logger.info("Cluster initialised and Session connected for keyspace '{}'", keyspace);
			return result;
		}
		catch (UnauthorizedException e) {
			String msg = "Fatal error. Cassandra user '" + username + "' is not authorized for keyspace '" + keyspace + "' : "
					+ e.getMessage();
			logger.error(msg, e);
			throw new CassandraConnectionException(msg, e);
		}
		catch (AuthenticationException e) {
			InetSocketAddress errorHost = e.getAddress();
			String msg = "Fatal error. Unable to authenticate Cassandra user '" + username + "' for keyspace '" + keyspace
					+ "' on Cassandra host '" + errorHost.toString() + "': " + e.getMessage();
			logger.error(msg, e);
			throw new CassandraConnectionException(msg, e);
		}
		catch (NoHostAvailableException e) {
			Map<InetSocketAddress, Throwable> errors = e.getErrors();
			StringBuilder errorMesages = new StringBuilder(
					"Unable to establish a client connection to any Cassandra node: " + e.getMessage() + ". Tried: ");
			if (errors != null && errors.size() > 0 && errors.keySet() != null) { // paranoid about null
				for (InetSocketAddress address : errors.keySet()) {
					Throwable throwable = errors.get(address);
					String stackTrace = stackTraceToString(throwable);
					errorMesages.append("\n\t" + address.toString() + " : " + throwable.getMessage());
					errorMesages.append("\n\t" + stackTrace);
				}
			}
			logger.error(errorMesages.toString(), e);
			throw new CassandraConnectionException(errorMesages.toString(), e);
		}
		catch (DriverException e) {
			String msg = "Unexpected DriverException encountered: " + e.getMessage();
			logger.error(msg, e);
			throw new CassandraConnectionException(msg, e);
		}
	}

	/**
	 * Converts a stacktrace to a String.
	 * 
	 * @param t
	 *            the Throwable to extract the stacktrace from
	 * 
	 * @return A String of the Throwable stacktrace
	 */
	private static String stackTraceToString(Throwable t) {
		StringWriter errors = new StringWriter();
		t.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	private static SSLOptions getSSLOptions(String truststoreFilePath, String truststorePass) throws CassandraConnectionException {
		File truststoreFile = new File(truststoreFilePath);
		if (truststoreFile.canRead() && truststoreFilePath.trim().length() > 0) {
			SSLContext context;
			try {
				context = getSSLContext(truststoreFile, truststorePass);
			}
			catch (Exception e) {
				throw new CassandraConnectionException("Can not read trust store file: " + truststoreFilePath + " ; " + e.getMessage());
			}
			String[] css = SSLOptions.DEFAULT_SSL_CIPHER_SUITES;

			return new SSLOptions(context, css);
		}
		return null;
	}

	private static SSLContext getSSLContext(File truststoreFile, String truststorePass) throws NoSuchAlgorithmException, KeyStoreException,
			CertificateException, IOException, UnrecoverableKeyException, KeyManagementException {

		FileInputStream tsf = null;
		SSLContext ctx = null;
		try {
			ctx = SSLContext.getInstance("SSL");

			tsf = new FileInputStream(truststoreFile);
			KeyStore ts = KeyStore.getInstance("JKS");
			ts.load(tsf, truststorePass.toCharArray());
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(ts);

			ctx.init(null, tmf.getTrustManagers(), new SecureRandom());

		}
		catch (Exception e) {
			logger.error("Problem encountered getting SSL Context " + e.getMessage(), e);
			e.printStackTrace();
		}
		finally {
			if (tsf != null) tsf.close();
		}
		return ctx;
	}

}
