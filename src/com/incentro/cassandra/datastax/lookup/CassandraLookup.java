/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.lookup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.exception.KettleValueException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMeta;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStep;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.incentro.cassandra.datastax.CassandraConnection;
import com.incentro.cassandra.datastax.CassandraConnectionException;
import com.incentro.cassandra.datastax.ConnectionCompression;
import com.incentro.cassandra.datastax.Utils;

/**
 * Class providing an input step for reading data from a Cassandra table (column family).
 * 
 * @author Rudmer van Dijk (rudmer{[dot]}vandijk{[at]}incentro{[dot]}com)
 */
public class CassandraLookup extends BaseStep implements StepInterface {

	private static final Class<?> PKG = CassandraLookup.class;

	private CassandraLookupMeta meta;
	private CassandraLookupData data;

	private List<String> inputRowColumns;

	private List<Integer> columnMapping;

	private CassandraConnection connection;

	private String nodes;

	private String port;

	private String username;

	private String password;

	private String keyspace;

	private Boolean withSSL;

	private String trustStoreFilePath;

	private String trustStorePass;

	private ConnectionCompression compression;

	private Map<String, ValueMetaInterface> rowColumnTypes = new HashMap<>();

	public CassandraLookup(StepMeta stepMeta, StepDataInterface stepDataInterface, int copyNr, TransMeta transMeta,
			Trans trans) {
		super(stepMeta, stepDataInterface, copyNr, transMeta, trans);
	}

	public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {
		Object[] r = getRow(); // Get row from input rowset & set row busy!
		if (r == null) { // no more input to be expected...
			setOutputDone(); // signal end to receiver(s)
			return false;
		}

		if (first) {
			first = false;
			meta = (CassandraLookupMeta) smi;
			data = (CassandraLookupData) sdi;

			data.inputRowMeta = getInputRowMeta().clone();
			data.outputRowMeta = getInputRowMeta().clone();
			meta.getFields(data.outputRowMeta, getStepname(), null, null, this, repository, metaStore);
			inputRowColumns = Arrays.asList(data.inputRowMeta.getFieldNames());
			columnMapping = new ArrayList<Integer>();
			for (String compareName : meta.keyCompareFieldName) {
				if (!Const.isEmpty(environmentSubstitute(compareName))) {
					columnMapping.add(inputRowColumns.indexOf(environmentSubstitute(compareName)));
				}
			}
			createQuery();
		}

		try {
			Object[] outputRow = lookupValues(data.inputRowMeta, r); // add new values to the row in rowset[0].
			putRow(data.outputRowMeta, outputRow); // copy row to output rowset(s);
		} catch (KettleException e) {
			logError(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Error.StepCanNotContinueForErrors", e.getMessage()));
			logError(Const.getStackTracker(e));
			setErrors(1);
			stopAll();
			setOutputDone(); // signal end to receiver(s)
			return false;
		}

		return true;
	}

	private Object[] lookupValues(RowMetaInterface inputRowMeta, Object[] r) {
		Object[] outputRow = new Object[data.outputRowMeta.size()];

		// Copy the results to the output row...
		// First copy the input row values to the output..
		//
		for (int i = 0; i < inputRowMeta.size(); i++) {
			outputRow[i] = r[i];
		}

		int outputIndex = inputRowMeta.size();

		StringBuilder sbCql = new StringBuilder();
		int columnMappingIndex = 0;
		for (StringBuilder cqlPart : data.ps) {
			sbCql.append(cqlPart);
			if (columnMappingIndex < columnMapping.size()) { // Loop will end with last part of sqlParts
				sbCql.append(r[columnMapping.get(columnMappingIndex)].toString());
				columnMappingIndex++;
			}
		}
		try {
			Row row = null;
			try {
				String cql = sbCql.toString();
				logDebug(cql);
				ResultSet results = getSession().execute(cql);
				row = results.one(); // Only get the first row
			}
			catch (NullPointerException e) {
				throw new KettleStepException("Failed to execute query, connection closed?");
			}

			if (row != null) {
				for (String name : meta.returnValueName) {
					Object rowField = row.getObject(name);
					if (rowField instanceof Integer) {
						rowField = new Long((Integer)rowField);
					}
					ValueMetaInterface fromMeta = rowColumnTypes.get(name);
					ValueMetaInterface toMeta = data.outputRowMeta.getValueMeta(outputIndex);
					if (fromMeta.getType() != toMeta.getType()) {
						rowField = toMeta.convertData(fromMeta, rowField);
					}
					outputRow[outputIndex++] = rowField;
				}
			}
		}
		catch (KettleStepException e) {
			logError("Could not retieve data: " + e.getMessage());
		}
		catch (KettleValueException e) {
			logError("Could not convert data: " + e.getMessage());
		}
		return outputRow;
	}

	public boolean init(StepMetaInterface smi, StepDataInterface sdi) {
		// Casting to step-specific implementation classes is safe
		meta = (CassandraLookupMeta) smi;
		data = (CassandraLookupData) sdi;

		nodes = environmentSubstitute(meta.getCassadraHost());
		port = environmentSubstitute(meta.getCassandraPort());
		username = environmentSubstitute(meta.getUsername());
		password = environmentSubstitute(meta.getPassword());
		keyspace = environmentSubstitute(meta.getKeyspace());
		withSSL = meta.getWithSSL();
		trustStoreFilePath = environmentSubstitute(meta.getTrustStoreFilePath());
		trustStorePass = environmentSubstitute(meta.getTrustStorePass());
		compression = meta.getCompression();

		try {
			this.connection = Utils.connect(nodes, port, username, password, keyspace, withSSL,
					trustStoreFilePath, trustStorePass, compression);
		} catch (CassandraConnectionException e) {
			logError("Could initialize step: " + e.getMessage());
			return false;
		}

		return super.init(meta, data);
	}

	@Override
	public void dispose(StepMetaInterface smi, StepDataInterface sdi) {
		if (connection != null)
			connection.release();
		super.dispose(meta, data);
	}

	public Session getSession() {
		return this.connection != null ? this.connection.getSession() : null;
	}

	public void createQuery() throws KettleStepException {
		Cluster cluster = getSession().getCluster();
		List<ColumnMetadata> tableColumns = cluster.getMetadata().getKeyspace(environmentSubstitute(meta.getKeyspace()))
				.getTable(environmentSubstitute(meta.getColumnFamily())).getColumns();
		HashMap<String, Boolean> fieldTypeIsTextMapping = new HashMap<String, Boolean>();

		for (ColumnMetadata columnRow : tableColumns) {
			int valueMetaType = Utils.convertDataType(columnRow.getType());
			ValueMetaInterface valueMeta = new ValueMeta(columnRow.getName(), valueMetaType);
			rowColumnTypes.put(columnRow.getName(), valueMeta);
			fieldTypeIsTextMapping.put(columnRow.getName(), valueMetaType == ValueMetaInterface.TYPE_STRING);
		}

		// Split up into parts because we will use a string builder later.
		// Better performance since String builder is faster that String.replaceFirst
		ArrayList<StringBuilder> cqlParts = new ArrayList<StringBuilder>();
		StringBuilder cqlPart = new StringBuilder();
		cqlPart.append("SELECT ");
		boolean first = true;
		for (String columnName : meta.returnValueName) {
			if (!first) {
				cqlPart.append(',');
			} else {
				first = false;
			}
			cqlPart.append(columnName);
		}
		cqlPart.append(" FROM ");
		cqlPart.append(meta.columnFamily);
		if (meta.keyName.size() > 0) {
			cqlPart.append(" WHERE ");
			for (int i = 0; i < meta.keyName.size(); i++) {
				if (i > 0) {
					cqlPart.append(" AND ");
				}

				cqlPart.append(meta.keyName.get(i));
				cqlPart.append(' ').append(meta.keyCondition.get(i)).append(' ');

				if (Const.isEmpty(meta.keyCompareFieldName.get(i))) {
					cqlPart.append('\'');
					cqlPart.append(meta.keyCompareValue.get(i));
					cqlPart.append('\'');
				}
				else {
					if (fieldTypeIsTextMapping.get(meta.keyName.get(i))) {
						cqlPart.append('\'');
					}
					cqlParts.add(cqlPart);
					cqlPart = new StringBuilder();
					if (fieldTypeIsTextMapping.get(meta.keyName.get(i))) {
						cqlPart.append('\'');
					}
				}
			}
		}
		cqlPart.append(';');
		cqlParts.add(cqlPart);
		logDetailed("Prepared CQL statement: " + cqlParts.toString());
		data.ps = cqlParts;
	}
}
