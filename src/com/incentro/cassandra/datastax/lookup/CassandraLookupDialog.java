/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.lookup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMeta;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.ui.core.dialog.ErrorDialog;
import org.pentaho.di.ui.core.widget.ColumnInfo;
import org.pentaho.di.ui.core.widget.TableView;
import org.pentaho.di.ui.core.widget.TextVar;
import org.pentaho.di.ui.trans.step.BaseStepDialog;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.TableMetadata;
import com.incentro.cassandra.datastax.CassandraConnection;
import com.incentro.cassandra.datastax.ConnectionCompression;
import com.incentro.cassandra.datastax.Utils;

public class CassandraLookupDialog extends BaseStepDialog implements StepDialogInterface {

	/**
	 * The PKG member is used when looking up internationalized strings.
	 * The properties file with localized keys is expected to reside in
	 * {the package of the class specified}/messages/messages_{locale}.properties
	 */
	public static final Class<?> PKG = CassandraLookupMeta.class;

	// this is the object the stores the step's settings
	// the dialog reads the settings from it when opening
	// the dialog writes the settings to it when confirmed
	private CassandraLookupMeta meta;

	// text field holding the name of the field to add to the row stream

	private Label wlHost;
	private TextVar wHost;

	private Label wlPort;
	private TextVar wPort;

	private Label wlFields;
	private TableView wFields;

	private HashMap<String, Integer> m_inputFields;
	private ColumnInfo[] colinf;
	private ColumnInfo[] colinf2;

	private Label wlOutputFields;
	private TableView wOutputFields;

	private Label wlUsername;
	private TextVar wUsername;

	private Label wlPassword;
	private TextVar wPassword;

	private Label wlKeyspace;
	private TextVar wKeyspace;

	private Label wlColumnFamily;
	private CCombo wColumnFamily;
	private Button wgetColumnFamilyBut;

	private Button wgetFieldsBut;
	private CassandraConnection connection;

	private List<ColumnMetadata> keyColumnsMeta;
	private ArrayList<String> keyColumns;
	private List<ColumnMetadata> columnsMeta;
	private ArrayList<String> columns;

	private Label wlWithSSL;
	private Button wWithSSL;

	private Label wlTruststorefile;
	private TextVar wTruststorefile;

	private Label wlTruststorepass;
	private TextVar wTruststorepass;

	private Label wlCompression;
	private CCombo wCompression;

	/**
	 * The constructor should simply invoke super() and save the incoming meta object to a local variable, so it can conveniently read and
	 * write settings from/to it.
	 * 
	 * @param parent
	 *            the SWT shell to open the dialog in
	 * @param in
	 *            the meta object holding the step's settings
	 * @param transMeta
	 *            transformation description
	 * @param sname
	 *            the step name
	 */
	public CassandraLookupDialog(org.eclipse.swt.widgets.Shell parent, java.lang.Object in, org.pentaho.di.trans.TransMeta transMeta,
			java.lang.String sname) {
		super(parent, (BaseStepMeta) in, transMeta, sname);
		meta = (CassandraLookupMeta) in;
		m_inputFields = new HashMap<String, Integer>();
	}

	/**
	 * This method is called by Spoon when the user opens the settings dialog of the step. It should open the dialog and return only once
	 * the dialog has been closed by the user.
	 * 
	 * If the user confirms the dialog, the meta object (passed in the constructor) must be updated to reflect the new step settings. The
	 * changed flag of the meta object must reflect whether the step configuration was changed by the dialog.
	 * 
	 * If the user cancels the dialog, the meta object must not be updated, and its changed flag must remain unaltered.
	 * 
	 * The open() method must return the name of the step after the user has confirmed the dialog, or null if the user cancelled the dialog.
	 */
	public String open() {

		// store some convenient SWT variables
		final Shell parent = getParent();
		final Display display = parent.getDisplay();

		// SWT code for preparing the dialog
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MIN | SWT.MAX);
		props.setLook(shell);
		setShellImage(shell, meta);

		// Save the value of the changed flag on the meta object. If the user
		// cancels
		// the dialog, it will be restored to this saved value.
		// The "changed" variable is inherited from BaseStepDialog
		changed = meta.hasChanged();

		// The ModifyListener used on all controls. It will update the meta
		// object to
		// indicate that changes are being made.
		ModifyListener lsMod = new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				meta.setChanged();
			}
		};

		// ------------------------------------------------------- //
		// SWT code for building the actual settings dialog //
		// ------------------------------------------------------- //
		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = Const.FORM_MARGIN;
		formLayout.marginHeight = Const.FORM_MARGIN;

		shell.setLayout(formLayout);
		shell.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookup.StepName"));

		final int middle = props.getMiddlePct();
		final int margin = Const.MARGIN;

		// Stepname line
		wlStepname = new Label(shell, SWT.RIGHT);
		wlStepname.setText(BaseMessages.getString(PKG, "System.Label.StepName"));
		props.setLook(wlStepname);
		fdlStepname = new FormData();
		fdlStepname.left = new FormAttachment(0, 0);
		fdlStepname.right = new FormAttachment(middle, -margin);
		fdlStepname.top = new FormAttachment(0, margin);
		wlStepname.setLayoutData(fdlStepname);

		wStepname = new Text(shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wStepname.setText(stepname);
		props.setLook(wStepname);
		wStepname.addModifyListener(lsMod);
		fdStepname = new FormData();
		fdStepname.left = new FormAttachment(middle, 0);
		fdStepname.top = new FormAttachment(0, margin);
		fdStepname.right = new FormAttachment(100, 0);
		wStepname.setLayoutData(fdStepname);

		// nodes line
		wlHost = new Label(shell, SWT.RIGHT);
		wlHost.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Hostname.Label"));
		props.setLook(wlHost);
		FormData fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wStepname, margin);
		wlHost.setLayoutData(fd);

		wHost = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wHost.setText(meta.getCassadraHost());
		props.setLook(wHost);
		wHost.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wStepname, margin);
		fd.right = new FormAttachment(100, 0);
		wHost.setLayoutData(fd);

		// port line
		wlPort = new Label(shell, SWT.RIGHT);
		wlPort.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Port.Label"));
		props.setLook(wlPort);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wHost, margin);
		wlPort.setLayoutData(fd);

		wPort = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wPort.setText(meta.getCassandraPort());
		props.setLook(wPort);
		wPort.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wHost, margin);
		fd.right = new FormAttachment(100, 0);
		wPort.setLayoutData(fd);

		// username line
		wlUsername = new Label(shell, SWT.RIGHT);
		wlUsername.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Username.Label"));
		props.setLook(wlUsername);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wPort, margin);
		wlUsername.setLayoutData(fd);

		wUsername = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wUsername.setText(meta.getUsername());
		props.setLook(wUsername);
		wUsername.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wPort, margin);
		fd.right = new FormAttachment(100, 0);
		wUsername.setLayoutData(fd);

		// password line
		wlPassword = new Label(shell, SWT.RIGHT);
		wlPassword.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Password.Label"));
		props.setLook(wlPassword);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wUsername, margin);
		wlPassword.setLayoutData(fd);

		wPassword = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wPassword.setText(meta.getPassword());
		props.setLook(wPassword);
		wPassword.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wUsername, margin);
		fd.right = new FormAttachment(100, 0);
		wPassword.setLayoutData(fd);

		// use SSL line
		wlWithSSL = new Label(shell, SWT.RIGHT);
		wlWithSSL.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.WithSSL.Label"));
		props.setLook(wlWithSSL);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wPassword, margin);
		wlWithSSL.setLayoutData(fd);

		wWithSSL = new Button(shell, SWT.CHECK);
		wWithSSL.setSelection(meta.getWithSSL());
		props.setLook(wWithSSL);
		fd = new FormData();
		fd.right = new FormAttachment(100, 0);
		fd.top = new FormAttachment(wPassword, margin);
		fd.left = new FormAttachment(middle, 0);
		wWithSSL.setLayoutData(fd);

		// Trust store file line
		wlTruststorefile = new Label(shell, SWT.RIGHT);
		props.setLook(wlTruststorefile);
		wlTruststorefile.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.TrustStoreFile.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wWithSSL, margin);
		fd.right = new FormAttachment(middle, -margin);
		wlTruststorefile.setLayoutData(fd);

		wTruststorefile = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wTruststorefile.setText(meta.getTrustStoreFilePath());
		props.setLook(wTruststorefile);
		wTruststorefile.addModifyListener(lsMod);
		fd = new FormData();
		fd.right = new FormAttachment(100, 0);
		fd.top = new FormAttachment(wWithSSL, margin);
		fd.left = new FormAttachment(middle, 0);
		wTruststorefile.setLayoutData(fd);

		// Trust store password line
		wlTruststorepass = new Label(shell, SWT.RIGHT | SWT.PASSWORD);
		props.setLook(wlTruststorepass);
		wlTruststorepass.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.TrustStorePassword.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wTruststorefile, margin);
		fd.right = new FormAttachment(middle, -margin);
		wlTruststorepass.setLayoutData(fd);

		wTruststorepass = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(wTruststorepass);
		wTruststorepass.setText(meta.getTrustStorePass());
		wTruststorepass.addModifyListener(lsMod);

		fd = new FormData();
		fd.right = new FormAttachment(100, 0);
		fd.top = new FormAttachment(wTruststorefile, margin);
		fd.left = new FormAttachment(middle, 0);
		wTruststorepass.setLayoutData(fd);

		// Compression
		wlCompression = new Label(shell, SWT.RIGHT);
		wlCompression.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Compression.Label"));
		wlCompression.setToolTipText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Compression.TipText"));
		props.setLook(wlCompression);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wTruststorepass, margin);
		fd.right = new FormAttachment(middle, -margin);
		wlCompression.setLayoutData(fd);

		wCompression = new CCombo(shell, SWT.BORDER | SWT.DROP_DOWN);
		// set fixed compression methods
		wCompression.add(ConnectionCompression.NONE.toString());
		wCompression.add(ConnectionCompression.SNAPPY.toString());
		try {
			CassandraLookupDialog.class.getClassLoader().loadClass("net.jpountz.lz4.LZ4Compressor");
			wCompression.add(ConnectionCompression.LZ4.toString());
		} catch (ClassNotFoundException e1) {
			// LZ4 not available
			wCompression.add(ConnectionCompression.LZ4.toString() + " (Not available)");
		}
		wCompression.setText(meta.getCompression().toString());
		wCompression.setEditable(false);
		wCompression.addModifyListener(lsMod);
		props.setLook(wCompression);
		wCompression.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				switch (ConnectionCompression.fromString(wCompression.getText())) {
				case NONE:
					wCompression.setToolTipText(
							BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.CompressionNone.TipText"));
					break;
				case SNAPPY:
					wCompression.setToolTipText(
							BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.CompressionSnappy.TipText"));
					break;
				case LZ4:
					wCompression.setToolTipText(
							BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.CompressionLZ4.TipText"));
					break;
				default:
					wCompression.setToolTipText(BaseMessages.getString(PKG,
							"DatastaxCassandraLookupDialog.CompressionLZ4NotAvailable.TipText"));
				}
			}
		});
		wCompression.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wTruststorepass, margin);
		// fd.right = new FormAttachment(100, 0);
		wCompression.setLayoutData(fd);

		// keyspace line
		wlKeyspace = new Label(shell, SWT.RIGHT);
		wlKeyspace.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.TargetKeyspace.Label"));
		props.setLook(wlKeyspace);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wCompression, margin);
		wlKeyspace.setLayoutData(fd);

		wKeyspace = new TextVar(transMeta, shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wKeyspace.setText(meta.getKeyspace());
		props.setLook(wKeyspace);
		wKeyspace.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wCompression, margin);
		fd.right = new FormAttachment(100, 0);
		wKeyspace.setLayoutData(fd);

		// column family line
		wlColumnFamily = new Label(shell, SWT.RIGHT);
		wlColumnFamily.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.TargetColumnFamily.Label"));
		props.setLook(wlColumnFamily);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wKeyspace, margin);
		wlColumnFamily.setLayoutData(fd);

		wgetColumnFamilyBut = new Button(shell, SWT.PUSH | SWT.CENTER);
		props.setLook(wgetColumnFamilyBut);
		wgetColumnFamilyBut.setText(' ' + BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.GetColumnFamily.Button") + ' ');
		fd = new FormData();
		fd.right = new FormAttachment(100, 0);
		fd.top = new FormAttachment(wKeyspace, margin);
		wgetColumnFamilyBut.setLayoutData(fd);

		wgetColumnFamilyBut.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setColumnFamily();
			}
		});

		wColumnFamily = new CCombo(shell, SWT.BORDER);
		wColumnFamily.setText(meta.getColumnFamily());
		props.setLook(wColumnFamily);
		wColumnFamily.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(wKeyspace, margin);
		fd.right = new FormAttachment(wgetColumnFamilyBut, -margin);
		wColumnFamily.setLayoutData(fd);

		// key fields line
		wlFields = new Label(shell, SWT.NONE);
		wlFields.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Keys.Label"));
		props.setLook(wlFields);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(wColumnFamily, margin * 4);
		wlFields.setLayoutData(fd);

		wgetFieldsBut = new Button(shell, SWT.PUSH | SWT.CENTER);
		props.setLook(wgetFieldsBut);
		wgetFieldsBut.setText(' ' + BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.GetFields.Button") + ' ');
		fd = new FormData();
		fd.right = new FormAttachment(100, 0);
		fd.top = new FormAttachment(wColumnFamily, margin * 2);
		wgetFieldsBut.setLayoutData(fd);

		wgetFieldsBut.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setColumns();
			}
		});

		colinf = new ColumnInfo[] {
				new ColumnInfo(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.ColumnInfo.Tablefield"), ColumnInfo.COLUMN_TYPE_CCOMBO,
						new String[] { "" }, true),
				new ColumnInfo(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.ColumnInfo.Comparator"), ColumnInfo.COLUMN_TYPE_CCOMBO,
						CassandraLookupMeta.conditionStrings, true),
				new ColumnInfo(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.ColumnInfo.StreamField"), ColumnInfo.COLUMN_TYPE_CCOMBO,
						new String[] { "" }, false),
				new ColumnInfo(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.ColumnInfo.Value"), ColumnInfo.COLUMN_TYPE_TEXT, false,
						false), };

		wFields = new TableView(transMeta, shell, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL, colinf, 1, false, lsMod, props);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wgetFieldsBut, margin);
		fd.right = new FormAttachment(100, 0);
		fd.bottom = new FormAttachment(100, -200);
		wFields.setLayoutData(fd);

		// Search the fields in the background
		final Runnable runnable = new Runnable() {
			public void run() {
				StepMeta stepMeta = transMeta.findStep(stepname);

				if (stepMeta != null) {
					try {
						RowMetaInterface row = transMeta.getPrevStepFields(stepMeta);

						// Remember these fields...
						for (int i = 0; i < row.size(); i++) {
							ValueMetaInterface field = row.getValueMeta(i);
							m_inputFields.put(field.getName(), Integer.valueOf(i));
						}

						setComboBoxes();
					}
					catch (KettleException e) {
						log.logError(toString(), BaseMessages.getString(PKG, "UnivariateStatsDialog.Log.UnableToFindInput"));
					}
				}
			}
		};

		new Thread(runnable).start();

		Table table = wFields.table;
		for (int i = 0; i < meta.keyName.size(); i++) {
			TableItem ti = new TableItem(table, SWT.NONE);
			ti.setText(1, meta.keyName.get(i));
			ti.setText(2, meta.keyCondition.get(i));
			ti.setText(3, meta.keyCompareFieldName.get(i));
			ti.setText(4, meta.keyCompareValue.get(i));
		}
		wFields.removeEmptyRows();
		wFields.setRowNums();
		wFields.optWidth(true);

		// lookup fields line
		wlOutputFields = new Label(shell, SWT.NONE);
		wlOutputFields.setText(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Return.Label")); // Steps:
		props.setLook(wlOutputFields);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wFields, margin);
		wlOutputFields.setLayoutData(fd);

		colinf2 = new ColumnInfo[] {
				new ColumnInfo(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.ColumnInfo.Tablefield"), ColumnInfo.COLUMN_TYPE_CCOMBO,
						new String[] { "" }, true),
				new ColumnInfo(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.ColumnInfo.Newname"), ColumnInfo.COLUMN_TYPE_TEXT, false,
						false),
				new ColumnInfo(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.ColumnInfo.Type"), ColumnInfo.COLUMN_TYPE_CCOMBO,
						ValueMetaInterface.typeCodes, true),
				new ColumnInfo(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.ColumnInfo.Default"), ColumnInfo.COLUMN_TYPE_TEXT, false,
						false), };

		wOutputFields = new TableView(transMeta, shell, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI, colinf2, 1, false, lsMod, props);

		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(wlOutputFields, margin);
		fd.right = new FormAttachment(100, 0);
		fd.bottom = new FormAttachment(100, -50);
		wOutputFields.setLayoutData(fd);

		table = wOutputFields.table;
		for (int i = 0; i < meta.returnValueName.size(); i++) {
			TableItem ti = new TableItem(table, SWT.NONE);
			ti.setText(1, meta.returnValueName.get(i));
			ti.setText(2, meta.returnValueNewName.get(i));
			ti.setText(3, ValueMeta.getTypeDesc(meta.returnValueDefaultType.get(i)));
			ti.setText(4, meta.returnValueDefault.get(i));
		}
		wOutputFields.removeEmptyRows();
		wOutputFields.setRowNums();
		wOutputFields.optWidth(true);

		// OK and cancel buttons
		wOK = new Button(shell, SWT.PUSH);
		wOK.setText(BaseMessages.getString(PKG, "System.Button.OK"));
		wCancel = new Button(shell, SWT.PUSH);
		wCancel.setText(BaseMessages.getString(PKG, "System.Button.Cancel"));

		BaseStepDialog.positionBottomButtons(shell, new Button[] { wOK, wCancel }, margin, wOutputFields);

		// Add listeners for cancel and OK
		lsCancel = new Listener() {
			public void handleEvent(Event e) {
				cancel();
			}
		};
		lsOK = new Listener() {
			public void handleEvent(Event e) {
				ok();
			}
		};

		wCancel.addListener(SWT.Selection, lsCancel);
		wOK.addListener(SWT.Selection, lsOK);

		// default listener (for hitting "enter")
		lsDef = new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
				ok();
			}
		};
		wStepname.addSelectionListener(lsDef);
		// wHelloFieldName.addSelectionListener(lsDef);

		// Detect X or ALT-F4 or something that kills this window and cancel the
		// dialog properly
		shell.addShellListener(new ShellAdapter() {
			public void shellClosed(ShellEvent e) {
				cancel();
			}
		});

		// Set/Restore the dialog size based on last position on screen
		// The setSize() method is inherited from BaseStepDialog
		setSize();

		// populate the dialog with the values from the meta object
		populateDialog();

		// restore the changed flag to original value, as the modify listeners
		// fire during dialog population
		meta.setChanged(changed);

		// open dialog and enter event loop
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) display.sleep();
		}

		// at this point the dialog has closed, so either ok() or cancel() have
		// been executed
		// The "stepname" variable is inherited from BaseStepDialog
		return stepname;
	}

	protected void setColumnFamily() {
		String nodes = transMeta.environmentSubstitute(wHost.getText());
		String port_s = transMeta.environmentSubstitute(wPort.getText());
		String username = transMeta.environmentSubstitute(wUsername.getText());
		String password = transMeta.environmentSubstitute(wPassword.getText());
		String keyspace = transMeta.environmentSubstitute(wKeyspace.getText());
		Boolean withSSL = wWithSSL.getSelection();
		String trustStoreFilePath = transMeta.environmentSubstitute(wTruststorefile.getText());
		String trustStorePass = transMeta.environmentSubstitute(wTruststorepass.getText());
		Cluster cluster;
		try {
			this.connection = Utils.connect(nodes, port_s, username, password, keyspace, withSSL, trustStoreFilePath,
					trustStorePass, ConnectionCompression.SNAPPY);
			cluster = this.connection.getSession().getCluster();
			Collection<TableMetadata> colFams = cluster.getMetadata().getKeyspace(keyspace).getTables();

			wColumnFamily.removeAll();
			Iterator<TableMetadata> iter2 = colFams.iterator();
			while (iter2.hasNext()) {
				TableMetadata row = iter2.next();
				wColumnFamily.add(row.getName());
			}
		}
		catch (Exception ex) {
			logError(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Error.ProblemGettingSchemaInfo.Message") + ":\n\n" + ex.getMessage(),
					ex);
			new ErrorDialog(shell, BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Error.ProblemGettingSchemaInfo.Title"),
					BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Error.ProblemGettingSchemaInfo.Message") + ":\n\n" + ex.getMessage(),
					ex);
		}
		finally {
			try {
				if (connection != null)
					connection.release();
			}
			catch (Exception e) {
				logError("Could not close connection after getting column families: " + e.getMessage());
			}
		}
	}

	protected void setColumns() {
		String nodes = transMeta.environmentSubstitute(wHost.getText());
		String port = transMeta.environmentSubstitute(wPort.getText());
		String username = transMeta.environmentSubstitute(wUsername.getText());
		String password = transMeta.environmentSubstitute(wPassword.getText());
		String keyspace = transMeta.environmentSubstitute(wKeyspace.getText());
		String columnFamily = transMeta.environmentSubstitute(wColumnFamily.getText());
		Boolean withSSL = wWithSSL.getSelection();
		String trustStoreFilePath = transMeta.environmentSubstitute(wTruststorefile.getText());
		String trustStorePass = transMeta.environmentSubstitute(wTruststorepass.getText());
		ConnectionCompression compression = ConnectionCompression.fromString(wCompression.getText());

		Cluster cluster;
		try {
			this.connection = Utils.connect(nodes, port, username, password, keyspace, withSSL, trustStoreFilePath,
					trustStorePass, compression);
			cluster = this.connection.getSession().getCluster();

			keyColumnsMeta = cluster.getMetadata().getKeyspace(keyspace).getTable(columnFamily).getPrimaryKey();
			keyColumns = new ArrayList<String>();
			Iterator<ColumnMetadata> iter = keyColumnsMeta.iterator();
			while (iter.hasNext()) {
				ColumnMetadata column = iter.next();
				keyColumns.add(column.getName());
			}

			String[] keyColumnsNames = (String[]) keyColumns.toArray(new String[keyColumns.size()]);
			Const.sortStrings(keyColumnsNames);
			colinf[0].setComboValues(keyColumnsNames);

			columnsMeta = cluster.getMetadata().getKeyspace(keyspace).getTable(columnFamily).getColumns();
			columns = new ArrayList<String>();
			Iterator<ColumnMetadata> iter2 = columnsMeta.iterator();
			while (iter2.hasNext()) {
				ColumnMetadata column = iter2.next();
				columns.add(column.getName());
			}

			String[] columnsNames = (String[]) columns.toArray(new String[columns.size()]);
			Const.sortStrings(columnsNames);
			colinf2[0].setComboValues(columnsNames);

		}
		catch (Exception ex) {
			logError(BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Error.ProblemGettingSchemaInfo.Message") + ":\n\n" + ex.getMessage(),
					ex);
			new ErrorDialog(shell, BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Error.ProblemGettingSchemaInfo.Title"),
					BaseMessages.getString(PKG, "DatastaxCassandraLookupDialog.Error.ProblemGettingSchemaInfo.Message") + ":\n\n" + ex.getMessage(),
					ex);
		}
		finally {
			try {
				if (connection != null)
					connection.release();
			}
			catch (Exception e) {
				logError("Could not close connection after getting column info: " + e.getMessage());
			}
		}

	}

	/**
	 * This helper method puts the step configuration stored in the meta object and puts it into the dialog controls.
	 */
	private void populateDialog() {
		wStepname.selectAll();
		// wHelloFieldName.setText(meta.getOutputFields());
	}

	/**
	 * Called when the user cancels the dialog.
	 */
	private void cancel() {
		// The "stepname" variable will be the return value for the open() method.
		// Setting to null to indicate that dialog was cancelled.
		stepname = null;
		// Restoring original "changed" flag on the meta object
		meta.setChanged(changed);
		// close the SWT dialog window
		dispose();
	}

	/**
	 * Called when the user confirms the dialog
	 */
	private void ok() {
		// The "stepname" variable will be the return value for the open() method.
		// Setting to step name from the dialog control
		stepname = wStepname.getText();
		// Setting the settings to the meta object
		meta.setCassadraHost(wHost.getText());
		meta.setCassandraPort(wPort.getText());
		meta.setUsername(wUsername.getText());
		meta.setPassword(wPassword.getText());
		meta.setKeyspace(wKeyspace.getText());
		meta.setColumnFamily(wColumnFamily.getText());
		meta.setWithSSL(wWithSSL.getSelection());
		meta.setTrustStoreFilePath(wTruststorefile.getText());
		meta.setTrustStorePass(wTruststorepass.getText());
		meta.setCompression(ConnectionCompression.fromString(wCompression.getText()));

		meta.keyName = new ArrayList<String>();
		meta.keyCondition = new ArrayList<String>();
		meta.keyCompareFieldName = new ArrayList<String>();
		meta.keyCompareValue = new ArrayList<String>();
		for (int i = 0; i < wFields.getItemCount(); i++) {
			meta.keyName.add(wFields.getItem(i, 1));
			meta.keyCondition.add(wFields.getItem(i, 2));
			meta.keyCompareFieldName.add(wFields.getItem(i, 3));
			meta.keyCompareValue.add(wFields.getItem(i, 4));
		}

		meta.returnValueName = new ArrayList<String>();
		meta.returnValueNewName = new ArrayList<String>();
		meta.returnValueDefaultType = new ArrayList<Integer>();
		meta.returnValueDefault = new ArrayList<String>();
		for (int i = 0; i < wOutputFields.getItemCount(); i++) {
			if (wOutputFields.getItem(i).length > 2) {
				meta.returnValueName.add(wOutputFields.getItem(i, 1));
				meta.returnValueNewName.add(wOutputFields.getItem(i, 2));
				meta.returnValueDefaultType.add(ValueMeta.getType(wOutputFields.getItem(i, 3)));
				meta.returnValueDefault.add(wOutputFields.getItem(i, 4));
			}
		}

		// close the SWT dialog window
		dispose();
	}

	/**
	 * Set up the input field combo box
	 */
	protected void setComboBoxes() {
		Set<String> keySet = m_inputFields.keySet();
		List<String> entries = new ArrayList<String>(keySet);
		String[] fieldNames = (String[]) entries.toArray(new String[entries.size()]);
		Const.sortStrings(fieldNames);
		colinf[2].setComboValues(fieldNames);
	}

	@Override
	public void dispose() {
		if (connection != null) {
			connection.release();
		}
		super.dispose();
	}
}
