/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.output;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.row.ValueMeta;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStep;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.exceptions.InvalidQueryException;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.incentro.cassandra.datastax.CassandraConnection;
import com.incentro.cassandra.datastax.ConnectionCompression;
import com.incentro.cassandra.datastax.Utils;

/**
 * Class providing an output step for writing data to a cassandra table (column
 * family).
 * 
 * @author Rudmer van Dijk (rudmer{[dot]}vandijk{[at]}incentro{[dot]}com)
 */
public class DatastaxCassandraOutput extends BaseStep implements StepInterface {

	protected CassandraConnection connection;

	protected DatastaxCassandraOutputMeta m_meta;
	protected DatastaxCassandraOutputData m_data;

	public DatastaxCassandraOutput(StepMeta stepMeta, StepDataInterface stepDataInterface, int copyNr,
			TransMeta transMeta, Trans trans) {

		super(stepMeta, stepDataInterface, copyNr, transMeta, trans);
	}

	/** Options for keyspace and row handlers */
	protected Map<String, String> m_opts;

	private List<ColumnMetadata> keyColumn;
	private HashMap<String, Integer> fieldMapping;
	private StringBuilder sb_insert = new StringBuilder();
	private String insert_ttl;
	private HashMap<String, Boolean> fieldTypeIsTextMapping;
	private boolean syncModeEnabled = false;
	private ResultSetFuture lastFuture;

	private String m_nodes;
	private String m_port;
	private String m_username;
	private String m_password;
	private String m_keyspaceName;
	private String m_columnFamilyName;
	private boolean m_withSSL;
	private String m_trustStoreFile;
	private String m_trustStorePassword;

	private ConnectionCompression m_compression;

	@Override
	public boolean init(StepMetaInterface smi, StepDataInterface sdi) {
		if (super.init(smi, sdi)) {
			m_meta = (DatastaxCassandraOutputMeta) smi;

			m_nodes = environmentSubstitute(m_meta.getCassandraHost());
			m_port = environmentSubstitute(m_meta.getCassandraPort());
			m_username = environmentSubstitute(m_meta.getUsername());
			m_password = environmentSubstitute(m_meta.getPassword());
			m_keyspaceName = environmentSubstitute(m_meta.getCassandraKeyspace());
			m_columnFamilyName = environmentSubstitute(m_meta.getColumnFamilyName());
			m_withSSL = m_meta.getSslEnabled();
			m_trustStoreFile = environmentSubstitute(m_meta.getTrustStoreFile());
			m_trustStorePassword = environmentSubstitute(m_meta.getTrustStorePassword());
			m_compression = m_meta.getCompression();
			syncModeEnabled = m_meta.getSyncModeEnabled();
			maxOpenQueue = syncModeEnabled ? 0 : m_meta.getBatchSize();

			try {
				if (Const.isEmpty(m_columnFamilyName)) {
					throw new RuntimeException(BaseMessages.getString(DatastaxCassandraOutputMeta.PKG,
							"CassandraOutput.Error.NoColumnFamilySpecified"));
				}
				if (m_meta.getSpecifyFields()
						&& m_meta.getCassandraFields().length != m_meta.getStreamFields().length) {
					throw new RuntimeException(BaseMessages.getString(DatastaxCassandraOutputMeta.PKG,
							"CassandraOutput.Error.InitializationColumnProblem"));
				}
				// open connection
				this.connection = Utils.connect(m_nodes, m_port, m_username, m_password,
						m_keyspaceName, m_withSSL, m_trustStoreFile, m_trustStorePassword, m_compression);
				// init successful
				return true;
			} catch (Exception ex) {
				logError(BaseMessages.getString(DatastaxCassandraOutputMeta.PKG,
						"DatastaxCassandraOutput.Error.InitializationProblem"), ex);
			}
		}
		return false;
	}

	protected void initialize(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {
		first = false;

		m_meta = (DatastaxCassandraOutputMeta) smi;
		m_data = (DatastaxCassandraOutputData) sdi;

		if (m_data == null || m_meta == null) {
			return;
		}

		// output (downstream) is the same as input
		m_data.setOutputRowMeta(getInputRowMeta());

		int m_ttl = m_meta.getTtl();

		try {
			Cluster cluster = connection.getSession().getCluster();

			Map<String, String> inputFields = new HashMap<String, String>();
			if (m_meta.getSpecifyFields()) {
				// custom field mapping
				for (int i = 0; i < m_meta.getCassandraFields().length; i++) {
					inputFields.put(m_meta.getCassandraFields()[i], m_meta.getStreamFields()[i]);
				}
			} else {
				// fields in stream match fields in database
				for (String s : m_data.getOutputRowMeta().getFieldNames()) {
					inputFields.put(s, s);
				}
			}

			try {
				// creating ArrayList with all column data from the selected Cassandra table
				fieldMapping = new LinkedHashMap<String, Integer>();
				fieldTypeIsTextMapping = new HashMap<String, Boolean>();

				List<ColumnMetadata> columns = cluster.getMetadata().getKeyspace(m_keyspaceName)
						.getTable(m_columnFamilyName).getColumns();
				Iterator<ColumnMetadata> iter = columns.iterator();
				while (iter.hasNext()) {
					ColumnMetadata columnRow = iter.next();

					fieldTypeIsTextMapping.put(columnRow.getName(),
							"TEXT".equals(columnRow.getType().getName().name()));

					if (inputFields.containsKey(columnRow.getName())) {
						// Put value columns in the fieldMapping collection if they also exist in Cassandra
						fieldMapping.put(columnRow.getName(),
								m_data.getOutputRowMeta().indexOfValue(inputFields.get(columnRow.getName())));

						if (!fieldTypeIsTextMapping.get(columnRow.getName())) {
							int index = m_data.getOutputRowMeta().indexOfValue(inputFields.get(columnRow.getName()));
							if ("INT".equals(columnRow.getType().getName().name()) && m_data.getOutputRowMeta()
									.getValueMeta(index).getType() != ValueMeta.TYPE_INTEGER) {
								throw new KettleStepException("Column " + columnRow.getName()
										+ " type ( INT ) in Cassandra does not match field type ( "
										+ m_data.getOutputRowMeta().getValueMeta(index).getType() + " ) in Pentaho ");
							} else if ("BIGINT".equals(columnRow.getType().getName().name()) && (m_data
									.getOutputRowMeta().getValueMeta(index).getType() != ValueMeta.TYPE_BIGNUMBER
									&& m_data.getOutputRowMeta().getValueMeta(index)
											.getType() != ValueMeta.TYPE_INTEGER)) {
								throw new KettleStepException("Column " + columnRow.getName()
										+ " type ( BIGINT ) in Cassandra does not match field type ( "
										+ m_data.getOutputRowMeta().getValueMeta(index).getType() + " ) in Pentaho ");
							}
						}
					}
				}

				// check fieldMapping if all keyColumns are used
				try {
					// get the column family meta data
					keyColumn = cluster.getMetadata().getKeyspace(m_keyspaceName).getTable(m_columnFamilyName).getPrimaryKey();
					Iterator<ColumnMetadata> keyIter = keyColumn.iterator();
					while (keyIter.hasNext()) {
						ColumnMetadata column = keyIter.next();
						if (!inputFields.containsKey(column.getName())) {
							throw new KettleStepException(BaseMessages.getString(DatastaxCassandraOutputMeta.PKG,
									"DatastaxCassandraOutput.Error.CantFindKeyField", column.getName()));
						}
					}
				} catch (Exception e) {
					logError(BaseMessages.getString(DatastaxCassandraOutputMeta.PKG,
							"DatastaxCassandraOutput.Error.InitializationColumnProblem"), e);
					throw e;
				}

				// Create the first part of the INSERT INTO statement
				sb_insert.append("INSERT INTO ").append(m_keyspaceName).append('.').append(m_columnFamilyName).append('(');

				int y = 1;
				for (String column : fieldMapping.keySet()) {
					sb_insert.append(column).append(fieldMapping.size() > y ? ',' : ')');
					y++;
				}

				sb_insert.append(" VALUES(");

				// prepare TTL option to append later to the CQL statement
				insert_ttl = m_ttl > 0 ? " USING TTL " + m_ttl : "";

			} catch (Exception e) {
				logError(BaseMessages.getString(DatastaxCassandraOutputMeta.PKG,
						"DatastaxCassandraOutput.Error.InitializationColumnProblem"), e);
				throw e;
			}

		} catch (Exception ex) {
			logError(BaseMessages.getString(DatastaxCassandraOutputMeta.PKG,
					"DatastaxCassandraOutput.Error.InitializationProblem"), ex);
		}
	}

	@Override
	public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {

		Object[] r = getRow();

		if (r == null || isStopped()) {

			// wait for last rows
			if (connection != null && !syncModeEnabled) {
				// do not accept new requests, lets current executions finish
				connection.getSession().closeAsync();
				processOpenFutures(0);
			}
			connection.release();

			setOutputDone();
			return false;
		}

		if (first) {
			initialize(smi, sdi);
		}

		StringBuilder finalInsert = new StringBuilder(sb_insert);
		// finish the INSERT INTO statement with the data from a single row.
		int i = 1;
		for (String columnRow : fieldMapping.keySet()) {
			Object field = r[fieldMapping.get(columnRow)];
			if (fieldTypeIsTextMapping.get(columnRow)) {
				finalInsert.append('\'');
				if (field instanceof String) {
					finalInsert.append(((String) field).replaceAll("'", "''"));
				} else {
					finalInsert.append(field);
				}
				finalInsert.append('\'');
			} else {
				finalInsert.append(field);
			}
			finalInsert.append(i < fieldMapping.size() ? ',' : ')');
			i++;
		}
		String cql = finalInsert.append(insert_ttl).append(';').toString();

		try {
			logRowlevel(cql);
			if (syncModeEnabled) {
				connection.getSession().execute(cql);
				incrementLinesOutput();
			} else {
				lastFuture = connection.getSession().executeAsync(cql);
				addFuture(lastFuture);
			}
			return true;
		} catch (InvalidQueryException e) {
			throw new KettleStepException("The data type of a field does not match the data type in cassandra: SQL: "
					+ cql + " :" + e.getMessage());
		} catch (NoHostAvailableException e) {
			int retryCount = 0;

			while (!isStopped() && retryCount++ < 3) {
				logError(
						"No Host Available Exception when doing insert, trying again in 30 seconds: " + e.getMessage());
				try {
					Thread.sleep(30000);
					this.connection = Utils.connect(m_nodes, m_port, m_username, m_password,
							m_keyspaceName, m_withSSL, m_trustStoreFile, m_trustStorePassword, m_compression);
					if (syncModeEnabled) {
						connection.getSession().execute(cql);
					} else {
						lastFuture = connection.getSession().executeAsync(cql);
					}
					return true;
				} catch (InterruptedException e1) {
					logError("Interrupted sleep while retrying query after No Host Available Exception: "
							+ e1.getMessage());
				} catch (NoHostAvailableException e1) {
					logError("No Host Available Exception while inserting, even after 30 seconds delayed retry: "
							+ e1.getMessage());
				} catch (InvalidQueryException e1) {
					throw new KettleStepException(
							"The data type of a field does not match the data type in cassandra: SQL: " + cql + " :"
									+ e1.getMessage());
				} catch (Exception e1) {
					throw new KettleException("Unknown error occured for CQL:\n" + cql, e1);
				}
			}
			if (retryCount == 3) {
				throw new KettleException("No more hosts available after 3 retries, aborting");
			}
		} catch (Exception e) {
			throw new KettleException("Unknown error occured for CQL:\n" + cql, e);
		}

		return false;
	}

	private List<ResultSetFuture> openFutures = new LinkedList<ResultSetFuture>();
	private int maxOpenQueue = 1000;

	private void addFuture(ResultSetFuture future) {
		openFutures.add(future);
		processOpenFutures(maxOpenQueue);
	}

	private void processOpenFutures(int maxOpen) {
		while (!isStopped() && hasTooManyOpenFutures(maxOpen)) {
			logDebug("waiting for max " + maxOpen + " open futures");
			try {
				Thread.sleep(128);
			} catch (InterruptedException e) {
				break;
			}
		}
	}

	private boolean hasTooManyOpenFutures(int maxOpen) {
		if (openFutures.size() > maxOpen) {
			// read futures and remove closed ones
			Iterator<ResultSetFuture> iter = openFutures.iterator();
			while (iter.hasNext()) {
				ResultSetFuture f = iter.next();
				if (f.isCancelled()) {
					incrementLinesRejected();
					iter.remove();
				} else if (f.isDone()) {
					incrementLinesOutput();
					iter.remove();
				}
			}
			return openFutures.size() > maxOpen;
		}
		return false;
	}

	@Override
	public void setStopped(boolean stopped) {
		if (isStopped() && stopped == true) {
			return;
		}
		super.setStopped(stopped);
	}

	@Override
	public void dispose(StepMetaInterface smi, StepDataInterface sdi) {
		if (connection != null)
			connection.release();
		super.dispose(smi, sdi);
	}

}
