/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.output;

import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;
import org.pentaho.di.core.CheckResult;
import org.pentaho.di.core.CheckResultInterface;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Counter;
import org.pentaho.di.core.annotations.Step;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.encryption.Encr;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.w3c.dom.Node;

import com.incentro.cassandra.datastax.ConnectionCompression;

/**
 * Class providing an output step for writing data to a cassandra table (column family).
 * 
 * @author Rudmer van Dijk (rudmer{[dot]}vandijk{[at]}incentro{[dot]}com)
 */
@Step( id = "DatastaxCassandraOutput"
	 , image = "DatastaxCassandraOutput.svg"
	 , name = "DatastaxCassandraOutput.StepName"
	 , i18nPackageName = "com.incentro.cassandra.datastax.output.messages"
	 , description = "DatastaxCassandraOutput.StepDescription"
	 , categoryDescription = "DatastaxCassandraOutput.StepCategory"
	 , documentationUrl = "http://htmlpreview.github.io/?https://bitbucket.org/incentro-ondemand/incentrodatastaxpentahoplugin/raw/c448401913bbb77836d5f98884cb70b1a25dadea/documentation/DatastaxCassandraOutput.html")
public class DatastaxCassandraOutputMeta extends BaseStepMeta implements StepMetaInterface {

	public static final Class<?> PKG = DatastaxCassandraOutputMeta.class;

	/** The host to contact */
	protected String m_cassandraHost = "localhost";

	/** The port that cassandra is listening on */
	protected String m_cassandraPort = "9042";

	/** The username to use for authentication */
	protected String m_username;

	/** The password to use for authentication */
	protected String m_password;

	/** Whether to use SSL or not **/
	protected boolean m_sslenabled;

	/** Whether to use sync load or not **/
	protected boolean m_syncModeEnabled;

	/** The trust store file location (String) **/
	public String m_truststorefile;

	/** The trust store password **/
	protected String m_truststorepassword;

	/** The keyspace (database) to use */
	protected String m_cassandraKeyspace;

	/** The column family (table) to write to */
	protected String m_columnFamily = "";

	/** if async mode is enabled this defines the maximum of open statements */
	protected int m_batchSize = 1000;

	/** query compression method to use in the driver */
	protected ConnectionCompression m_compression;

	/** enables list with field mapping */
	protected boolean m_specifyFields = false;
	protected String[] m_streamFields;
	protected String[] m_cassandraFields;

	/** specify Time To Live for each row */
	protected int m_ttl = 0;

	/**
	 * Set the trust store file location
	 * 
	 * @param trustStoreFile
	 *            location of the trust store file
	 */
	public void setTrustStoreFile(String trustStoreFile) {
		m_truststorefile = trustStoreFile;
	}

	/**
	 * Get the trust store file location
	 * 
	 * @return location of the trust store file
	 */
	public String getTrustStoreFile() {
		return m_truststorefile;
	}

	/**
	 * Set the trust store password
	 * 
	 * @param trustStorePassword
	 *            password of the trust store
	 */
	public void setTrustStorePassword(String trustStorePassword) {
		m_truststorepassword = trustStorePassword;
	}

	/**
	 * Get the trust store password
	 * 
	 * @return password of the trust store
	 */
	public String getTrustStorePassword() {
		return m_truststorepassword;
	}

	/**
	 * Set whether to use SSL or not
	 * 
	 * @param sslEnabled
	 *            true if SSL is enabled
	 */
	public void setSslEnabled(boolean sslEnabled) {
		m_sslenabled = sslEnabled;
	}

	/**
	 * Get whether SSL is enabled or not
	 * 
	 * @return true if SSL is enabled
	 */
	public boolean getSslEnabled() {
		return m_sslenabled;
	}

	/**
	 * Set whether to use sync load or not
	 * 
	 * @param syncEnabled
	 *            true if sync load is enabled
	 */
	public void setSyncModeEnabled(boolean syncEnabled) {
		m_syncModeEnabled = syncEnabled;
	}

	/**
	 * Get whether sync load is enabled or not
	 * 
	 * @return true if sync load is enabled
	 */
	public boolean getSyncModeEnabled() {
		return m_syncModeEnabled;
	}

	/**
	 * Set the cassandra node hostname to connect to
	 * 
	 * @param host
	 *            the host to connect to
	 */
	public void setCassandraHost(String host) {
		m_cassandraHost = host;
	}

	/**
	 * Get the name of the cassandra node to connect to
	 * 
	 * @return the name of the cassandra node to connect to
	 */
	public String getCassandraHost() {
		return m_cassandraHost;
	}

	/**
	 * Set the port that cassandra is listening on
	 * 
	 * @param port
	 *            the port that cassandra is listening on
	 */
	public void setCassandraPort(String port) {
		m_cassandraPort = port;
	}

	/**
	 * Get the port that cassandra is listening on
	 * 
	 * @return the port that cassandra is listening on
	 */
	public String getCassandraPort() {
		return m_cassandraPort;
	}

	/**
	 * Set the username to authenticate with
	 * 
	 * @param un
	 *            the username to authenticate with
	 */
	public void setUsername(String un) {
		m_username = un;
	}

	/**
	 * Get the username to authenticate with
	 * 
	 * @return the username to authenticate with
	 */
	public String getUsername() {
		return m_username;
	}

	/**
	 * Set the password to authenticate with
	 * 
	 * @param pass
	 *            the password to authenticate with
	 */
	public void setPassword(String pass) {
		m_password = pass;
	}

	/**
	 * Get the password to authenticate with
	 * 
	 * @return the password to authenticate with
	 */
	public String getPassword() {
		return m_password;
	}

	/**
	 * Set the keyspace (db) to use
	 * 
	 * @param keyspace
	 *            the keyspace to use
	 */
	public void setCassandraKeyspace(String keyspace) {
		m_cassandraKeyspace = keyspace;
	}

	/**
	 * Get the keyspace (db) to use
	 * 
	 * @return the keyspace (db) to use
	 */
	public String getCassandraKeyspace() {
		return m_cassandraKeyspace;
	}

	/**
	 * Set the column family (table) to write to
	 * 
	 * @param colFam
	 *            the name of the column family to write to
	 */
	public void setColumnFamilyName(String colFam) {
		m_columnFamily = colFam;
	}

	/**
	 * Get the name of the column family to write to
	 * 
	 * @return the name of the column family to write to
	 */
	public String getColumnFamilyName() {
		return m_columnFamily;
	}

	/**
	 * Set the 'batch' size for inserts, it defines the maximum of open
	 * statements
	 * 
	 * @param batchSize
	 *            the limit to use for open insert statements
	 */
	public void setBatchSize(int batchSize) {
		this.m_batchSize = batchSize;
	}

	/**
	 * Get the 'batch' size or maximum open insert statements
	 * 
	 * @return the limit to use for open insert statements
	 */
	public int getBatchSize() {
		return m_batchSize;
	}

	/**
	 * @return the compression method to use
	 */
	public ConnectionCompression getCompression() {
		return m_compression;
	}

	/**
	 * @param compression
	 *            the Compression method to use
	 */
	public void setCompression(ConnectionCompression compression) {
		this.m_compression = compression;
	}

	/**
	 * @return the m_specifyFields
	 */
	public boolean getSpecifyFields() {
		return m_specifyFields;
	}

	/**
	 * @param m_specifyFields
	 *            the m_specifyFields to set
	 */
	public void setSpecifyFields(boolean specifyFields) {
		this.m_specifyFields = specifyFields;
	}

	/**
	 * @return the m_streamFields
	 */
	public String[] getStreamFields() {
		return m_streamFields;
	}

	/**
	 * @param m_streamFields
	 *            the m_streamFields to set
	 */
	public void setStreamFields(String[] streamFields) {
		this.m_streamFields = streamFields;
	}

	/**
	 * @return the m_cassandraFields
	 */
	public String[] getCassandraFields() {
		return m_cassandraFields;
	}

	/**
	 * @param m_cassandraFields
	 *            the m_cassandraFields to set
	 */
	public void setCassandraFields(String[] cassandraFields) {
		this.m_cassandraFields = cassandraFields;
	}

	/**
	 * @return the ttl for each row
	 */
	public int getTtl() {
		return m_ttl;
	}

	/**
	 * @param ttl
	 *            the TTL for each row to set
	 */
	public void setTtl(int ttl) {
		this.m_ttl = ttl;
	}

	/**
	 * Initialise the arrays for field mapping
	 * 
	 * @param nrRows
	 *            the size of the mapping
	 */
	public void allocate(int nrRows) {
		m_streamFields = new String[nrRows];
		m_cassandraFields = new String[nrRows];
	}

	@Override
	public String getXML() {
		StringBuffer retval = new StringBuffer();

		if (!Const.isEmpty(m_cassandraHost)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_host", m_cassandraHost));
		}

		if (!Const.isEmpty(m_cassandraPort)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_port", m_cassandraPort));
		}

		if (!Const.isEmpty(m_password)) {
			retval.append("    ")
					.append(XMLHandler.addTagValue("password", Encr.encryptPasswordIfNotUsingVariables(m_password)));
		}

		if (!Const.isEmpty(m_username)) {
			retval.append("    ").append(XMLHandler.addTagValue("username", m_username));
		}

		if (!Const.isEmpty(m_cassandraKeyspace)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_keyspace", m_cassandraKeyspace));
		}

		retval.append("    ").append(XMLHandler.addTagValue("ssl_enabled", m_sslenabled));

		if (!Const.isEmpty(m_truststorefile)) {
			retval.append("    ").append(XMLHandler.addTagValue("truststorefile", m_truststorefile));
		}

		if (!Const.isEmpty(m_truststorepassword)) {
			retval.append("    ").append(XMLHandler.addTagValue("truststorepassword",
					Encr.encryptPasswordIfNotUsingVariables(m_truststorepassword)));
		}

		if (!Const.isEmpty(m_columnFamily)) {
			retval.append("    ").append(XMLHandler.addTagValue("column_family", m_columnFamily));
		}

		retval.append("    ").append(XMLHandler.addTagValue("sync_enabled", m_syncModeEnabled));
		retval.append("    ").append(XMLHandler.addTagValue("batch_size", m_batchSize));
		retval.append("    ").append(XMLHandler.addTagValue("query_compression", m_compression.toString()));
		retval.append("    ").append(XMLHandler.addTagValue("specify_fields", m_specifyFields));

		if (m_specifyFields) {
			retval.append("    <field_mapping>");
			for (int i = 0; i < m_cassandraFields.length; i++) {
				retval.append("      <field>");
				retval.append("        ").append(XMLHandler.addTagValue("column_name", m_cassandraFields[i]));
				retval.append("        ").append(XMLHandler.addTagValue("stream_name", m_streamFields[i]));
				retval.append("      </field>");
			}
			retval.append("    </field_mapping>");
		}

		retval.append("    ").append(XMLHandler.addTagValue("ttl", m_ttl));

		return retval.toString();
	}

	@Override
	public void loadXML(Node stepnode, List<DatabaseMeta> databases, Map<String, Counter> counters)
			throws KettleXMLException {
		m_cassandraHost = XMLHandler.getTagValue(stepnode, "cassandra_host");
		m_cassandraPort = XMLHandler.getTagValue(stepnode, "cassandra_port");
		m_username = XMLHandler.getTagValue(stepnode, "username");
		m_password = XMLHandler.getTagValue(stepnode, "password");
		if (!Const.isEmpty(m_password)) {
			m_password = Encr.decryptPasswordOptionallyEncrypted(m_password);
		}
		m_cassandraKeyspace = XMLHandler.getTagValue(stepnode, "cassandra_keyspace");
		m_sslenabled = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "ssl_enabled"));
		m_truststorefile = XMLHandler.getTagValue(stepnode, "truststorefile");
		m_truststorepassword = XMLHandler.getTagValue(stepnode, "truststorepassword");
		if (!Const.isEmpty(m_truststorepassword)) {
			m_truststorepassword = Encr.decryptPasswordOptionallyEncrypted(m_truststorepassword);
		}
		m_columnFamily = XMLHandler.getTagValue(stepnode, "column_family");
		m_syncModeEnabled = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "sync_enabled"));

		String batchSize = XMLHandler.getTagValue(stepnode, "batch_size");
		m_batchSize = Const.isEmpty(batchSize) ? 1000 : Integer.valueOf(batchSize).intValue();

		String sCompression = XMLHandler.getTagValue(stepnode, "query_compression");
		m_compression = Const.isEmpty(sCompression) ? ConnectionCompression.SNAPPY : ConnectionCompression.fromString(sCompression);

		m_specifyFields = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "specify_fields"));

		Node fields = XMLHandler.getSubNode(stepnode, "field_mapping");
		int nrRows = XMLHandler.countNodes(fields, "field");

		allocate(nrRows);
		for (int i = 0; i < nrRows; i++) {
			Node knode = XMLHandler.getSubNodeByNr(fields, "field", i);

			m_cassandraFields[i] = XMLHandler.getTagValue(knode, "column_name");
			m_streamFields[i] = XMLHandler.getTagValue(knode, "stream_name");
		}

		m_ttl = Const.toInt(XMLHandler.getTagValue(stepnode, "ttl"), 0);
	}

	@Override
	public void readRep(Repository rep, ObjectId id_step, List<DatabaseMeta> databases, Map<String, Counter> counters)
			throws KettleException {
		m_cassandraHost = rep.getStepAttributeString(id_step, 0, "cassandra_host");
		m_cassandraPort = rep.getStepAttributeString(id_step, 0, "cassandra_port");
		m_username = rep.getStepAttributeString(id_step, 0, "username");
		m_password = rep.getStepAttributeString(id_step, 0, "password");
		if (!Const.isEmpty(m_password)) {
			m_password = Encr.decryptPasswordOptionallyEncrypted(m_password);
		}
		m_cassandraKeyspace = rep.getStepAttributeString(id_step, 0, "cassandra_keyspace");
		m_sslenabled = rep.getStepAttributeBoolean(id_step, 0, "ssl_enabled");
		m_truststorefile = rep.getStepAttributeString(id_step, 0, "truststorefile");
		m_truststorepassword = rep.getStepAttributeString(id_step, 0, "truststorepassword");
		if (!Const.isEmpty(m_truststorepassword)) {
			m_truststorepassword = Encr.decryptPasswordOptionallyEncrypted(m_truststorepassword);
		}
		m_columnFamily = rep.getStepAttributeString(id_step, 0, "column_family");
		m_syncModeEnabled = rep.getStepAttributeBoolean(id_step, 0, "sync_enabled");

		String batchSize = rep.getStepAttributeString(id_step, 0, "batch_size");
		m_batchSize = Const.isEmpty(batchSize) ? 1000 : Integer.valueOf(batchSize).intValue();

		String sCompression = rep.getStepAttributeString(id_step, 0, "query_compression");
		m_compression = Const.isEmpty(sCompression) ? ConnectionCompression.SNAPPY : ConnectionCompression.fromString(sCompression);

		m_specifyFields = rep.getStepAttributeBoolean(id_step, 0, "specify_fields");

		int nrCols = rep.countNrStepAttributes(id_step, "column_name");
		int nrStreams = rep.countNrStepAttributes(id_step, "stream_name");
		int nrRows = (nrCols < nrStreams ? nrStreams : nrCols);

		allocate(nrRows);
		for (int idx = 0; idx < nrRows; idx++) {
			m_cassandraFields[idx] = Const.NVL(rep.getStepAttributeString(id_step, idx, "column_name"), "");
			m_streamFields[idx] = Const.NVL(rep.getStepAttributeString(id_step, idx, "stream_name"), "");
		}

		m_ttl = Const.toInt(rep.getStepAttributeString(id_step, 0, "ttl"), 0);
	}

	@Override
	public void saveRep(Repository rep, ObjectId id_transformation, ObjectId id_step) throws KettleException {
		if (!Const.isEmpty(m_cassandraHost)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_host", m_cassandraHost);
		}

		if (!Const.isEmpty(m_cassandraPort)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_port", m_cassandraPort);
		}

		if (!Const.isEmpty(m_username)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "username", m_username);
		}

		if (!Const.isEmpty(m_password)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "password",
					Encr.encryptPasswordIfNotUsingVariables(m_password));
		}

		if (!Const.isEmpty(m_cassandraKeyspace)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_keyspace", m_cassandraKeyspace);
		}

		rep.saveStepAttribute(id_transformation, id_step, 0, "ssl_enabled", m_sslenabled);

		if (!Const.isEmpty(m_truststorefile)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "truststorefile", m_truststorefile);
		}

		if (!Const.isEmpty(m_truststorepassword)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "truststorepassword",
					Encr.encryptPasswordIfNotUsingVariables(m_truststorepassword));
		}

		if (!Const.isEmpty(m_columnFamily)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "column_family", m_columnFamily);
		}

		rep.saveStepAttribute(id_transformation, id_step, 0, "sync_enabled", m_syncModeEnabled);

		rep.saveStepAttribute(id_transformation, id_step, 0, "batch_size", m_batchSize);

		if (m_compression != null) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "query_compression", m_compression.toString());
		}

		rep.saveStepAttribute(id_transformation, id_step, 0, "specify_fields", m_specifyFields);

		if (m_specifyFields) {
			int nrRows = (m_cassandraFields.length < m_streamFields.length ? m_streamFields.length
					: m_cassandraFields.length);
			for (int idx = 0; idx < nrRows; idx++) {
				String columnName = (idx < m_cassandraFields.length ? m_cassandraFields[idx] : "");
				String streamName = (idx < m_streamFields.length ? m_streamFields[idx] : "");
				rep.saveStepAttribute(id_transformation, id_step, idx, "column_name", columnName);
				rep.saveStepAttribute(id_transformation, id_step, idx, "stream_name", streamName);
			}
		}

		rep.saveStepAttribute(id_transformation, id_step, 0, "ttl", m_ttl);
	}

	@Override
	public void check(List<CheckResultInterface> remarks, TransMeta transMeta, StepMeta stepMeta, RowMetaInterface prev,
			String[] input, String[] output, RowMetaInterface info) {

		CheckResult cr;

		if ((prev == null) || (prev.size() == 0)) {
			cr = new CheckResult(CheckResult.TYPE_RESULT_WARNING, "Not receiving any fields from previous steps!",
					stepMeta);
			remarks.add(cr);
		} else {
			cr = new CheckResult(CheckResult.TYPE_RESULT_OK,
					"Step is connected to previous one, receiving " + prev.size() + " fields", stepMeta);
			remarks.add(cr);
		}

		// See if we have input streams leading to this step!
		if (input.length > 0) {
			cr = new CheckResult(CheckResult.TYPE_RESULT_OK, "Step is receiving info from other steps.", stepMeta);
			remarks.add(cr);
		} else {
			cr = new CheckResult(CheckResult.TYPE_RESULT_ERROR, "No input received from other steps!", stepMeta);
			remarks.add(cr);
		}

		if (m_sslenabled && ((Const.isEmpty(m_truststorefile)) || (Const.isEmpty(m_truststorepassword)))) {
			cr = new CheckResult(CheckResult.TYPE_RESULT_ERROR,
					"SSL is enabled but the trust storefile or/and password is not entered", stepMeta);
			remarks.add(cr);
		}

		if (Const.isEmpty(m_cassandraKeyspace)) {
			cr = new CheckResult(CheckResult.TYPE_RESULT_ERROR, "No keyspace specified!", stepMeta);
			remarks.add(cr);
		}

		if (Const.isEmpty(m_columnFamily)) {
			cr = new CheckResult(CheckResult.TYPE_RESULT_ERROR, "No column family (table) specified!", stepMeta);
			remarks.add(cr);
		}

	}

	@Override
	public StepInterface getStep(StepMeta stepMeta, StepDataInterface stepDataInterface, int copyNr,
			TransMeta transMeta, Trans trans) {

		return new DatastaxCassandraOutput(stepMeta, stepDataInterface, copyNr, transMeta, trans);
	}

	@Override
	public StepDataInterface getStepData() {
		return new DatastaxCassandraOutputData();
	}

	@Override
	public void setDefault() {
		m_cassandraHost = "localhost";
		m_cassandraPort = "9042";
		m_columnFamily = "";
		m_sslenabled = false;
		m_truststorefile = "";
		m_truststorepassword = "";
		m_syncModeEnabled = false;
		m_batchSize = 1000;
		m_compression = ConnectionCompression.SNAPPY;
		m_specifyFields = false;
		m_streamFields = new String[0];
		m_cassandraFields = new String[0];
		m_ttl = 0;
	}

	@Override
	public String getDialogClassName() {
		return DatastaxCassandraOutputDialog.class.getName();
	}

	public StepDialogInterface getDialog(Shell shell, StepMetaInterface meta, TransMeta transMeta, String name) {
		return new DatastaxCassandraOutputDialog(shell, meta, transMeta, name);
	}

	@Override
	public boolean supportsErrorHandling() {
		return false;
	}
}
