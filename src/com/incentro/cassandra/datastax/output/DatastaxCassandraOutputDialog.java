/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.output;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Props;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.ui.core.dialog.ErrorDialog;
import org.pentaho.di.ui.core.widget.ColumnInfo;
import org.pentaho.di.ui.core.widget.TableView;
import org.pentaho.di.ui.core.widget.TextVar;
import org.pentaho.di.ui.trans.step.BaseStepDialog;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.TableMetadata;
import com.incentro.cassandra.datastax.CassandraConnection;
import com.incentro.cassandra.datastax.ConnectionCompression;
import com.incentro.cassandra.datastax.Utils;

/**
 * Dialog class for the CassandraOutput step
 * 
 * @author Rudmer van Dijk (rudmer{[dot]}vandijk{[at]}incentro{[dot]}com)
 */
public class DatastaxCassandraOutputDialog extends BaseStepDialog implements StepDialogInterface {

	private static final Class<?> PKG = DatastaxCassandraOutputMeta.class;

	private final DatastaxCassandraOutputMeta m_currentMeta;
	private final DatastaxCassandraOutputMeta m_originalMeta;

	/** various UI bits and pieces for the dialog */
	private Label m_stepnameLabel;
	private Text m_stepnameText;

	private CTabFolder m_wTabFolder;
	private CTabItem m_connectionTab;
	private CTabItem m_writeTab;

	private Label m_hostLab;
	private TextVar m_hostText;
	private Label m_portLab;
	private TextVar m_portText;

	private Label m_userLab;
	private TextVar m_userText;
	private Label m_passLab;
	private TextVar m_passText;

	private Label m_keyspaceLab;
	private TextVar m_keyspaceText;

	private Label m_columnFamilyLab;
	private CCombo m_columnFamilyCombo;
	private Button m_getColumnFamiliesBut;

	private Label m_sslenabledLab;
	private Button m_sslenabledBut;

	private Label m_syncModeEnabledLab;
	private Button m_syncModeEnabledBut;

	private Label m_truststorefileLab;
	private TextVar m_truststorefileText;

	private Label m_truststorepassLab;
	private TextVar m_truststorepassText;

	private Label m_batchSizeLab;
	private TextVar m_batchSizeText;

	private Label m_useCompressionLab;
	private CCombo m_useCompressionCombo;

	private Label m_specifyFieldsLab;
	private Button m_specifyFieldsBut;

	private Label m_fieldsLabel;
	private TableView m_fieldsList;
	private Button m_getFieldsBut;
	private ColumnInfo[] ciFields;
	private final Map<String, Integer> inputFields = new HashMap<String, Integer>();

	private Label m_ttlLab;
	private TextVar m_ttlText;

	private CassandraConnection connection = null;

	public DatastaxCassandraOutputDialog(Shell parent, Object in, TransMeta tr, String name) {

		super(parent, (BaseStepMeta) in, tr, name);

		m_currentMeta = (DatastaxCassandraOutputMeta) in;
		m_originalMeta = (DatastaxCassandraOutputMeta) m_currentMeta.clone();
	}

	@Override
	public String open() {

		final Shell parent = getParent();
		final Display display = parent.getDisplay();

		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MIN | SWT.MAX);

		props.setLook(shell);
		setShellImage(shell, m_currentMeta);

		// used to listen to a text field (m_wStepname)
		final ModifyListener lsMod = new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				m_currentMeta.setChanged();
			}
		};

		changed = m_currentMeta.hasChanged();

		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = Const.FORM_MARGIN;
		formLayout.marginHeight = Const.FORM_MARGIN;

		shell.setLayout(formLayout);
		shell.setText(stepname);
		props.setLook(shell);

		final int middle = props.getMiddlePct();
		final int margin = Const.MARGIN;

		// Stepname line
		m_stepnameLabel = new Label(shell, SWT.RIGHT);
		m_stepnameLabel.setText(BaseMessages.getString(PKG, "System.Label.StepName"));
		props.setLook(m_stepnameLabel);

		FormData fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(middle, -margin);
		fd.top = new FormAttachment(0, margin);
		m_stepnameLabel.setLayoutData(fd);
		m_stepnameText = new Text(shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		m_stepnameText.setText(stepname);
		props.setLook(m_stepnameText);
		m_stepnameText.addModifyListener(lsMod);

		// format the text field
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(0, margin);
		fd.right = new FormAttachment(100, 0);
		m_stepnameText.setLayoutData(fd);

		// arrange fields into tabs
		m_wTabFolder = new CTabFolder(shell, SWT.BORDER);
		props.setLook(m_wTabFolder, Props.WIDGET_STYLE_TAB);
		m_wTabFolder.setSimple(false);

		/////////////////////////////////////////////////
		// start of the connection tab
		m_connectionTab = new CTabItem(m_wTabFolder, SWT.BORDER);
		m_connectionTab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.Tab.Connection"));

		Composite wConnectionComp = new Composite(m_wTabFolder, SWT.NONE);
		props.setLook(wConnectionComp);
		wConnectionComp.setLayout(formLayout);

		// host line
		m_hostLab = new Label(wConnectionComp, SWT.RIGHT);
		props.setLook(m_hostLab);
		m_hostLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.Hostname.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(0, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_hostLab.setLayoutData(fd);

		m_hostText = new TextVar(transMeta, wConnectionComp, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(m_hostText);
		m_hostText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				m_hostText.setToolTipText(transMeta.environmentSubstitute(m_hostText.getText()));
			}
		});
		m_hostText.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(0, margin);
		fd.right = new FormAttachment(100, 0);
		m_hostText.setLayoutData(fd);

		// port line
		m_portLab = new Label(wConnectionComp, SWT.RIGHT);
		props.setLook(m_portLab);
		m_portLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.Port.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_hostText, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_portLab.setLayoutData(fd);

		m_portText = new TextVar(transMeta, wConnectionComp, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(m_portText);
		m_portText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				m_portText.setToolTipText(transMeta.environmentSubstitute(m_portText.getText()));
			}
		});
		m_portText.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_hostText, margin);
		fd.right = new FormAttachment(100, 0);
		m_portText.setLayoutData(fd);

		// username line
		m_userLab = new Label(wConnectionComp, SWT.RIGHT);
		props.setLook(m_userLab);
		m_userLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.User.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_portText, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_userLab.setLayoutData(fd);

		m_userText = new TextVar(transMeta, wConnectionComp, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(m_userText);
		m_userText.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_portText, margin);
		fd.right = new FormAttachment(100, 0);
		m_userText.setLayoutData(fd);

		// password line
		m_passLab = new Label(wConnectionComp, SWT.RIGHT | SWT.PASSWORD);
		props.setLook(m_passLab);
		m_passLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.Password.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_userText, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_passLab.setLayoutData(fd);

		m_passText = new TextVar(transMeta, wConnectionComp, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(m_passText);
		m_passText.addModifyListener(lsMod);

		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_userText, margin);
		fd.right = new FormAttachment(100, 0);
		m_passText.setLayoutData(fd);

		// keyspace line
		m_keyspaceLab = new Label(wConnectionComp, SWT.RIGHT);
		props.setLook(m_keyspaceLab);
		m_keyspaceLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.Keyspace.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_passText, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_keyspaceLab.setLayoutData(fd);

		m_keyspaceText = new TextVar(transMeta, wConnectionComp, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(m_keyspaceText);
		m_keyspaceText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				m_keyspaceText.setToolTipText(transMeta.environmentSubstitute(m_keyspaceText.getText()));
			}
		});
		m_keyspaceText.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_passText, margin);
		fd.right = new FormAttachment(100, 0);
		m_keyspaceText.setLayoutData(fd);

		// ssl enabled line
		m_sslenabledLab = new Label(wConnectionComp, SWT.RIGHT);
		m_sslenabledLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.SSLEnabled.Label"));
		m_sslenabledLab.setToolTipText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.SSLEnabled.TipText"));
		props.setLook(m_sslenabledLab);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_keyspaceText, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_sslenabledLab.setLayoutData(fd);

		m_sslenabledBut = new Button(wConnectionComp, SWT.CHECK);
		props.setLook(m_sslenabledBut);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_keyspaceText, margin);
		fd.right = new FormAttachment(100, 0);
		m_sslenabledBut.setLayoutData(fd);

		// Trust store file line
		m_truststorefileLab = new Label(wConnectionComp, SWT.RIGHT);
		props.setLook(m_truststorefileLab);
		m_truststorefileLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.TrustStoreFile.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_sslenabledBut, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_truststorefileLab.setLayoutData(fd);

		m_truststorefileText = new TextVar(transMeta, wConnectionComp, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(m_truststorefileText);
		m_truststorefileText.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_sslenabledBut, margin);
		fd.right = new FormAttachment(100, 0);
		m_truststorefileText.setLayoutData(fd);

		// trust store password line
		m_truststorepassLab = new Label(wConnectionComp, SWT.RIGHT | SWT.PASSWORD);
		props.setLook(m_truststorepassLab);
		m_truststorepassLab
				.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.TrustStorePassword.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_truststorefileText, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_truststorepassLab.setLayoutData(fd);

		m_truststorepassText = new TextVar(transMeta, wConnectionComp, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		props.setLook(m_truststorepassText);
		m_truststorepassText.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_truststorefileText, margin);
		fd.right = new FormAttachment(100, 0);
		m_truststorepassText.setLayoutData(fd);

		// sync mode enabled line
		m_syncModeEnabledLab = new Label(wConnectionComp, SWT.RIGHT);
		m_syncModeEnabledLab
				.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.SyncModeEnabled.Label"));
		m_syncModeEnabledLab
				.setToolTipText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.SyncModeEnabled.TipText"));
		props.setLook(m_syncModeEnabledLab);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_truststorepassText, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_syncModeEnabledLab.setLayoutData(fd);

		m_syncModeEnabledBut = new Button(wConnectionComp, SWT.CHECK);
		props.setLook(m_syncModeEnabledBut);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_truststorepassText, margin);
		fd.right = new FormAttachment(100, 0);
		m_syncModeEnabledBut.setLayoutData(fd);

		// batch mode lines
		m_batchSizeLab = new Label(wConnectionComp, SWT.RIGHT);
		m_batchSizeLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.BatchSize.Label"));
		m_batchSizeLab.setToolTipText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.BatchSize.TipText"));
		props.setLook(m_batchSizeLab);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_syncModeEnabledBut, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_batchSizeLab.setLayoutData(fd);

		m_batchSizeText = new TextVar(transMeta, wConnectionComp, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		m_batchSizeText.addModifyListener(lsMod);
		props.setLook(m_batchSizeText);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_syncModeEnabledBut, margin);
		fd.right = new FormAttachment(100, 0);
		m_batchSizeText.setLayoutData(fd);

		// query compression
		m_useCompressionLab = new Label(wConnectionComp, SWT.RIGHT);
		m_useCompressionLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.UseCompression.Label"));
		m_useCompressionLab
				.setToolTipText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.UseCompression.TipText"));
		props.setLook(m_useCompressionLab);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_batchSizeText, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_useCompressionLab.setLayoutData(fd);

		m_useCompressionCombo = new CCombo(wConnectionComp, SWT.BORDER);
		// set fixed compression methods
		m_useCompressionCombo.add(ConnectionCompression.NONE.toString());
		m_useCompressionCombo.add(ConnectionCompression.SNAPPY.toString());
		try {
			DatastaxCassandraOutputDialog.class.getClassLoader().loadClass("net.jpountz.lz4.LZ4Compressor");
			m_useCompressionCombo.add(ConnectionCompression.LZ4.toString());
		} catch (ClassNotFoundException e1) {
			// LZ4 not available
			m_useCompressionCombo.add(ConnectionCompression.LZ4.toString() + " (Not available)");
		}
		m_useCompressionCombo.setEditable(false);
		m_useCompressionCombo.addModifyListener(lsMod);
		props.setLook(m_useCompressionCombo);
		m_useCompressionCombo.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				switch (ConnectionCompression.fromString(m_useCompressionCombo.getText())) {
				case NONE:
					m_useCompressionCombo.setToolTipText(
							BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.CompressionNone.TipText"));
					break;
				case SNAPPY:
					m_useCompressionCombo.setToolTipText(
							BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.CompressionSnappy.TipText"));
					break;
				case LZ4:
					m_useCompressionCombo.setToolTipText(
							BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.CompressionLZ4.TipText"));
					break;
				default:
					m_useCompressionCombo.setToolTipText(BaseMessages.getString(PKG,
							"DatastaxCassandraOutputDialog.CompressionLZ4NotAvailable.TipText"));
				}
			}
		});
		m_useCompressionCombo.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_batchSizeText, margin);
		// fd.right = new FormAttachment(100, 0);
		m_useCompressionCombo.setLayoutData(fd);

		// connection tab layout and placement
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(0, 0);
		fd.right = new FormAttachment(100, 0);
		wConnectionComp.setLayoutData(fd);

		wConnectionComp.layout();
		m_connectionTab.setControl(wConnectionComp);

		/////////////////////////////////////////////////
		// start of the write options tab
		m_writeTab = new CTabItem(m_wTabFolder, SWT.BORDER);
		m_writeTab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.Tab.WriteOptions"));

		Composite wWriteComp = new Composite(m_wTabFolder, SWT.NONE);
		props.setLook(wWriteComp);
		wWriteComp.setLayout(formLayout);

		// column family line
		m_columnFamilyLab = new Label(wWriteComp, SWT.RIGHT);
		props.setLook(m_columnFamilyLab);
		m_columnFamilyLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.ColumnFamily.Label"));
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(0, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_columnFamilyLab.setLayoutData(fd);

		m_getColumnFamiliesBut = new Button(wWriteComp, SWT.PUSH | SWT.CENTER);
		props.setLook(m_getColumnFamiliesBut);
		m_getColumnFamiliesBut.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.GetColFam.Button"));
		fd = new FormData();
		fd.top = new FormAttachment(0, margin);
		fd.right = new FormAttachment(100, 0);
		m_getColumnFamiliesBut.setLayoutData(fd);
		m_getColumnFamiliesBut.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setupColumnFamiliesCombo();
			}
		});

		m_columnFamilyCombo = new CCombo(wWriteComp, SWT.BORDER);
		props.setLook(m_columnFamilyCombo);
		m_columnFamilyCombo.addModifyListener(lsMod);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(0, margin);
		fd.right = new FormAttachment(m_getColumnFamiliesBut, -margin);
		m_columnFamilyCombo.setLayoutData(fd);

		// TTL for each row
		m_ttlLab = new Label(wWriteComp, SWT.RIGHT);
		m_ttlLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.TTL.Label"));
		props.setLook(m_ttlLab);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_columnFamilyCombo, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_ttlLab.setLayoutData(fd);

		m_ttlText = new TextVar(transMeta, wWriteComp, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		m_ttlText.setToolTipText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.TTL.TipText"));
		m_ttlText.addModifyListener(lsMod);
		props.setLook(m_ttlText);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_columnFamilyCombo, margin);
		fd.right = new FormAttachment(100, 0);
		m_ttlText.setLayoutData(fd);

		// Specify fields
		m_specifyFieldsLab = new Label(wWriteComp, SWT.RIGHT);
		m_specifyFieldsLab.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.SpecifyFields.Label"));
		props.setLook(m_specifyFieldsLab);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_ttlText, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_specifyFieldsLab.setLayoutData(fd);

		m_specifyFieldsBut = new Button(wWriteComp, SWT.CHECK);
		props.setLook(m_specifyFieldsBut);
		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_ttlText, margin);
		fd.right = new FormAttachment(100, 0);
		m_specifyFieldsBut.setLayoutData(fd);

		// If the flag is off, gray out the fields tab e.g.
		m_specifyFieldsBut.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				setFlags();
			}
		});

		// The fields table
		m_fieldsLabel = new Label(wWriteComp, SWT.RIGHT);
		m_fieldsLabel.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.OutputFields.Label"));
		props.setLook(m_fieldsLabel);
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_specifyFieldsBut, margin);
		fd.right = new FormAttachment(middle, -margin);
		m_fieldsLabel.setLayoutData(fd);

		m_getFieldsBut = new Button(wWriteComp, SWT.PUSH);
		m_getFieldsBut.setText(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.GetFields.Button"));
		fd = new FormData();
		fd.right = new FormAttachment(100, 0);
		fd.top = new FormAttachment(m_specifyFieldsBut, margin);
		m_getFieldsBut.setLayoutData(fd);

		int tableCols = 2;
		int UpInsRows = (m_currentMeta.getStreamFields() != null ? m_currentMeta.getStreamFields().length : 1);
		ciFields = new ColumnInfo[tableCols];
		ciFields[0] = new ColumnInfo(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.ColumnInfo.TableField"),
				ColumnInfo.COLUMN_TYPE_CCOMBO, new String[] { "" }, false);
		ciFields[1] = new ColumnInfo(
				BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.ColumnInfo.StreamField"),
				ColumnInfo.COLUMN_TYPE_CCOMBO, new String[] { "" }, false);
		m_fieldsList = new TableView(transMeta, wWriteComp,
				SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL, ciFields, UpInsRows, lsMod,
				props);

		fd = new FormData();
		fd.left = new FormAttachment(middle, 0);
		fd.top = new FormAttachment(m_specifyFieldsBut, margin);
		fd.right = new FormAttachment(m_getFieldsBut, -margin);
		fd.bottom = new FormAttachment(100, -margin);
		m_fieldsList.setLayoutData(fd);

		// write options tab layout and placement
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(0, 0);
		fd.right = new FormAttachment(100, 0);
		wWriteComp.setLayoutData(fd);

		wWriteComp.layout();
		m_writeTab.setControl(wWriteComp);

		// Buttons inherited from BaseStepDialog
		wOK = new Button(shell, SWT.PUSH);
		wOK.setText(BaseMessages.getString(PKG, "System.Button.OK"));

		wCancel = new Button(shell, SWT.PUSH);
		wCancel.setText(BaseMessages.getString(PKG, "System.Button.Cancel"));

		setButtonPositions(new Button[] { wOK, wCancel }, margin, null);

		// tab folder placement
		fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.top = new FormAttachment(m_stepnameText, margin);
		fd.right = new FormAttachment(100, 0);
		fd.bottom = new FormAttachment(wOK, -margin);
		m_wTabFolder.setLayoutData(fd);

		// Add listeners
		lsCancel = new Listener() {
			@Override
			public void handleEvent(Event e) {
				cancel();
			}
		};

		lsOK = new Listener() {
			@Override
			public void handleEvent(Event e) {
				ok();
			}
		};

		lsGet = new Listener() {
			@Override
			public void handleEvent(Event e) {
				get();
			}
		};

		wCancel.addListener(SWT.Selection, lsCancel);
		wOK.addListener(SWT.Selection, lsOK);
		m_getFieldsBut.addListener(SWT.Selection, lsGet);

		lsDef = new SelectionAdapter() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				ok();
			}
		};

		m_stepnameText.addSelectionListener(lsDef);

		// Detect X or ALT-F4 or something that kills this window...
		shell.addShellListener(new ShellAdapter() {
			@Override
			public void shellClosed(ShellEvent e) {
				cancel();
			}
		});

		m_wTabFolder.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if (m_writeTab.equals(arg0.item)) {
					setStreamFieldCombo();
					setTableFieldCombo();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});

		m_wTabFolder.setSelection(0);
		m_stepnameText.setSelection(0);
		setSize();

		getData();

		// add this listener after loading data so it will not trigger a cassandra connection
		m_columnFamilyCombo.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				m_columnFamilyCombo.setToolTipText(transMeta.environmentSubstitute(m_columnFamilyCombo.getText()));
				setTableFieldCombo();
			}
		});

		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		return stepname;
	}

	protected void setupColumnFamiliesCombo() {

		// Get the connection to Cassandra
		String nodes = transMeta.environmentSubstitute(m_hostText.getText());
		String port_s = transMeta.environmentSubstitute(m_portText.getText());
		String username = transMeta.environmentSubstitute(m_userText.getText());
		String password = transMeta.environmentSubstitute(m_passText.getText());
		String keyspace = transMeta.environmentSubstitute(m_keyspaceText.getText());
		Boolean withSSL = m_sslenabledBut.getSelection();
		String truststorefile = transMeta.environmentSubstitute(m_truststorefileText.getText());
		String truststorepass = transMeta.environmentSubstitute(m_truststorepassText.getText());
		ConnectionCompression compression = ConnectionCompression.fromString(m_useCompressionCombo.getText());
		Cluster cluster = null;
		try {
			this.connection = Utils.connect(nodes, port_s, username, password, keyspace, withSSL,
					truststorefile, truststorepass, compression);
			cluster = this.connection.getSession().getCluster();
			Collection<TableMetadata> colFams = cluster.getMetadata()
					.getKeyspace(transMeta.environmentSubstitute(m_keyspaceText.getText())).getTables();

			m_columnFamilyCombo.removeAll();
			Iterator<TableMetadata> iter = colFams.iterator();
			while (iter.hasNext()) {
				TableMetadata row = iter.next();
				m_columnFamilyCombo.add(row.getName());
			}

		} catch (Exception ex) {
			logError(BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.Error.ProblemGettingSchemaInfo.Message")
					+ ":\n\n" + ex.getMessage(), ex);
			new ErrorDialog(shell,
					BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.Error.ProblemGettingSchemaInfo.Title"),
					BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.Error.ProblemGettingSchemaInfo.Message")
							+ ":\n\n" + ex.getMessage(), ex);
		} finally {
			if (connection != null)
				connection.release();
		}
	}

	private void setTableFieldCombo() {
		Runnable fieldLoader = new Runnable() {
			public void run() {
				if (!shell.isDisposed()) {
					final String nodes = transMeta.environmentSubstitute(m_hostText.getText());
					final String port_s = transMeta.environmentSubstitute(m_portText.getText());
					final String username = transMeta.environmentSubstitute(m_userText.getText());
					final String password = transMeta.environmentSubstitute(m_passText.getText());
					final String keyspace = transMeta.environmentSubstitute(m_keyspaceText.getText());
					final Boolean withSSL = m_sslenabledBut.getSelection();
					final String truststorefile = transMeta.environmentSubstitute(m_truststorefileText.getText());
					final String truststorepass = transMeta.environmentSubstitute(m_truststorepassText.getText());
					final ConnectionCompression compression = ConnectionCompression.fromString(m_useCompressionCombo.getText());
					final String columnFamily = m_columnFamilyCombo.getText();

					// clear
					ciFields[0].setComboValues(new String[] {});
					if (!Const.isEmpty(columnFamily)) {
						String[] fieldNames = null;
						try {
							// try to get table info
							connection = Utils.connect(nodes, port_s, username, password, keyspace, withSSL,
									truststorefile, truststorepass, compression);
							Cluster cluster = connection.getSession().getCluster();
							Metadata clusterMeta = cluster != null ? cluster.getMetadata() : null;
							KeyspaceMetadata keyspaceMeta = clusterMeta != null ? clusterMeta.getKeyspace(keyspace) : null;
							TableMetadata tableMeta = keyspaceMeta != null ? keyspaceMeta.getTable(columnFamily) : null;

							if (tableMeta != null) {
								List<ColumnMetadata> column = tableMeta.getColumns();
								Iterator<ColumnMetadata> iter = column.iterator();
								fieldNames = new String[column.size()];
								int i = 0;
								while (iter.hasNext()) {
									fieldNames[i++] = iter.next().getName();
								}
							}
						} catch (Exception e) {
							// silently ignore errors, lists will be empty
						}
						if (!m_fieldsList.isDisposed()) {
							ciFields[0].setComboValues(fieldNames);
						}
					}
					if (connection != null)
						connection.release();
				}
			}
		};
		shell.getDisplay().asyncExec(fieldLoader);
	}

	private void setStreamFieldCombo() {
		Runnable fieldLoader = new Runnable() {
			public void run() {
				try {
					StepMeta stepMeta = transMeta.findStep(stepname);
					RowMetaInterface row = transMeta.getPrevStepFields(stepMeta);

					if (row != null && inputFields.size() == 0) {
						// Remember these fields...
						for (int i = 0; i < row.size(); i++) {
							inputFields.put(row.getValueMeta(i).getName(), i);
						}
					}
				} catch (KettleException e) {
					logError(BaseMessages.getString(PKG, "System.Dialog.GetFieldsFailed.Message"));
				}

				// Add the currentMeta fields...
				Set<String> keySet = inputFields.keySet();
				List<String> entries = new ArrayList<String>(keySet);

				String[] fieldNames = entries.toArray(new String[entries.size()]);

				Const.sortStrings(fieldNames);
				if (!m_fieldsList.isDisposed()) {
					ciFields[1].setComboValues(fieldNames);
				}
			}
		};
		shell.getDisplay().asyncExec(fieldLoader);
	}

	protected void setFlags() {
		boolean specifyFields = m_specifyFieldsBut.getSelection();
		m_fieldsList.setEnabled(specifyFields);
		m_getFieldsBut.setEnabled(specifyFields);
	}

	protected void ok() {
		if (Const.isEmpty(m_stepnameText.getText())) {
			return;
		}

		stepname = m_stepnameText.getText();
		m_currentMeta.setCassandraHost(m_hostText.getText());
		m_currentMeta.setCassandraPort(m_portText.getText());
		m_currentMeta.setUsername(m_userText.getText());
		m_currentMeta.setPassword(m_passText.getText());
		m_currentMeta.setCassandraKeyspace(m_keyspaceText.getText());
		m_currentMeta.setColumnFamilyName(m_columnFamilyCombo.getText());

		if (!m_originalMeta.equals(m_currentMeta)) {
			m_currentMeta.setChanged();
			changed = m_currentMeta.hasChanged();
		}

		m_currentMeta.setSslEnabled(m_sslenabledBut.getSelection());
		m_currentMeta.setTrustStoreFile(m_truststorefileText.getText());
		m_currentMeta.setTrustStorePassword(m_truststorepassText.getText());
		m_currentMeta.setSyncModeEnabled(m_syncModeEnabledBut.getSelection());

		String batchSize = m_batchSizeText.getText();
		m_currentMeta.setBatchSize(Const.isEmpty(batchSize) ? 0 : Integer.valueOf(batchSize).intValue());
		m_currentMeta.setCompression(ConnectionCompression.fromString(m_useCompressionCombo.getText()));

		m_currentMeta.setTtl(Const.toInt(m_ttlText.getText(), 0));

		m_currentMeta.setSpecifyFields(m_specifyFieldsBut.getSelection());
		int nrRows = m_fieldsList.nrNonEmpty();
		m_currentMeta.allocate(nrRows);
		String[] streamFields = m_currentMeta.getStreamFields();
		String[] cassandraFields = m_currentMeta.getCassandraFields();
		for (int i = 0; i < nrRows; i++) {
			TableItem item = m_fieldsList.getNonEmpty(i);
			cassandraFields[i] = Const.NVL(item.getText(1), "");
			streamFields[i] = Const.NVL(item.getText(2), "");
		}
		dispose();
	}

	protected void cancel() {
		stepname = null;
		m_currentMeta.setChanged(changed);

		dispose();
	}

	/**
	 * Fill up the fields table with the incoming fields.
	 */
	private void get() {
		try {
			RowMetaInterface r = transMeta.getPrevStepFields(stepname);
			if (r != null && !r.isEmpty()) {
				BaseStepDialog.getFieldsFromPrevious(r, m_fieldsList, 1, new int[] { 1, 2 }, new int[] {}, -1, -1,
						null);
			}
		} catch (KettleException e) {
			new ErrorDialog(shell,
					BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.FailedToGetFields.DialogTitle"),
					BaseMessages.getString(PKG, "DatastaxCassandraOutputDialog.FailedToGetFields.DialogMessage"), e);
		}
	}

	protected void getData() {

		if (!Const.isEmpty(m_currentMeta.getCassandraHost())) {
			m_hostText.setText(m_currentMeta.getCassandraHost());
		}

		if (!Const.isEmpty(m_currentMeta.getCassandraPort())) {
			m_portText.setText(m_currentMeta.getCassandraPort());
		}

		if (!Const.isEmpty(m_currentMeta.getUsername())) {
			m_userText.setText(m_currentMeta.getUsername());
		}

		if (!Const.isEmpty(m_currentMeta.getPassword())) {
			m_passText.setText(m_currentMeta.getPassword());
		}

		if (!Const.isEmpty(m_currentMeta.getCassandraKeyspace())) {
			m_keyspaceText.setText(m_currentMeta.getCassandraKeyspace());
		}

		if (!Const.isEmpty(m_currentMeta.getColumnFamilyName())) {
			m_columnFamilyCombo.setText(m_currentMeta.getColumnFamilyName());
		}

		m_sslenabledBut.setSelection(m_currentMeta.getSslEnabled());

		m_syncModeEnabledBut.setSelection(m_currentMeta.getSyncModeEnabled());

		if (!Const.isEmpty(m_currentMeta.m_truststorefile)) {
			m_truststorefileText.setText(m_currentMeta.getTrustStoreFile());
		}

		if (!Const.isEmpty(m_currentMeta.getTrustStorePassword())) {
			m_truststorepassText.setText(m_currentMeta.getTrustStorePassword());
		}

		m_batchSizeText.setText(String.valueOf(m_currentMeta.getBatchSize()));
		m_useCompressionCombo.setText(m_currentMeta.getCompression().toString());

		m_ttlText.setText(String.valueOf(m_currentMeta.getTtl()));
		m_specifyFieldsBut.setSelection(m_currentMeta.getSpecifyFields());

		for (int i = 0; i < m_currentMeta.getCassandraFields().length; i++) {
			TableItem item = m_fieldsList.table.getItem(i);
			if (m_currentMeta.getCassandraFields()[i] != null) {
				item.setText(1, m_currentMeta.getCassandraFields()[i]);
			}
			if (m_currentMeta.getStreamFields()[i] != null) {
				item.setText(2, m_currentMeta.getStreamFields()[i]);
			}
		}
		m_fieldsList.setRowNums();
		m_fieldsList.optWidth(true);

		setFlags();
	}

	@Override
	public void dispose() {
		if (connection != null) {
			connection.release();
		}
		super.dispose();
	}
}
