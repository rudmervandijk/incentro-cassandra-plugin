/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.input;

import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.step.BaseStepData;
import org.pentaho.di.trans.step.StepDataInterface;

public class CassandraInputData extends BaseStepData implements StepDataInterface {

	/** The input data format */
	protected RowMetaInterface inputRowMeta;
	/** The output data format */
	protected RowMetaInterface outputRowMeta;

	/**
	 * Get the input row format
	 * 
	 * @return the input row format
	 */
	public RowMetaInterface getInputRowMeta() {
		return inputRowMeta;
	}

	/**
	 * Set the input row format
	 * 
	 * @param rmi
	 *            the input row format
	 */
	public void setInputRowMeta(RowMetaInterface rmi) {
		inputRowMeta = rmi;
	}

	/**
	 * Get the output row format
	 * 
	 * @return the output row format
	 */
	public RowMetaInterface getOutputRowMeta() {
		return outputRowMeta;
	}

	/**
	 * Set the output row format
	 * 
	 * @param rmi
	 *            the output row format
	 */
	public void setOutputRowMeta(RowMetaInterface rmi) {
		outputRowMeta = rmi;
	}
}
