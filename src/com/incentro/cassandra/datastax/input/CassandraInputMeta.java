/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax.input;

import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Counter;
import org.pentaho.di.core.annotations.Step;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.encryption.Encr;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMeta;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.core.variables.VariableSpace;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.metastore.api.IMetaStore;
import org.w3c.dom.Node;

import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.incentro.cassandra.datastax.CassandraConnection;
import com.incentro.cassandra.datastax.CassandraConnectionException;
import com.incentro.cassandra.datastax.ConnectionCompression;
import com.incentro.cassandra.datastax.Utils;

@Step( id = "DatastaxCassandraInput"
     , image = "DatastaxCassandraInput.svg"
     , name = "DatastaxCassandraInput.StepName"
     , i18nPackageName = "com.incentro.cassandra.datastax.input.messages"
     , description = "DatastaxCassandraInput.StepDescription"
     , categoryDescription = "DatastaxCassandraInput.StepCategory"
     , documentationUrl = "http://htmlpreview.github.io/?https://bitbucket.org/incentro-ondemand/incentrodatastaxpentahoplugin/raw/4d147ca57e631d0374e607d5f33f4d6d751d31e0/documentation/DatastaxCassandraInput.html")
public class CassandraInputMeta extends BaseStepMeta implements StepMetaInterface {

	/** The cassandra nodes to contact */
	protected String cassandraNodes = "localhost";

	/** The port that cassandra is listening on */
	protected String cassandraPort = "9042";

	/** The username to use for authentication */
	protected String username;

	/** The password to use for authentication */
	protected String password;

	/** Whether to use SSL or not **/
	public Boolean withSSL;

	/** The trust store file location (String) **/
	public String trustStoreFilePath;

	/** The trust store password **/
	public String trustStorePass;

	/** query compression method to use in the driver */
	protected ConnectionCompression compression;

	/** The keyspace (database) to use */
	public String keyspace;

	/** the CQL statement to execute */
	protected String cqlStatement;

	/** limit the number of rows to retrieve */
	protected int rowLimit = 0;

	/** generate a CQL statement for each row of input */
	protected Boolean executeForEachInputRow;

	@Override
	public String getXML() {
		StringBuffer retval = new StringBuffer();

		if (!Const.isEmpty(cassandraNodes)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_nodes", cassandraNodes));
		}

		if (!Const.isEmpty(cassandraPort)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_port", cassandraPort));
		}

		if (!Const.isEmpty(username)) {
			retval.append("    ").append(XMLHandler.addTagValue("username", username));
		}

		if (!Const.isEmpty(password)) {
			retval.append("    ").append(XMLHandler.addTagValue("password", Encr.encryptPasswordIfNotUsingVariables(password)));
		}

		if (!Const.isEmpty(keyspace)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_keyspace", keyspace));
		}

		if (withSSL != null) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_withSSL", (withSSL ? "Y" : "N")));
		}

		if (!Const.isEmpty(trustStoreFilePath)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_trustStoreFilePath", trustStoreFilePath));
		}

		if (!Const.isEmpty(trustStorePass)) {
			retval.append("    ").append(XMLHandler.addTagValue("cassandra_trustStorePass", Encr.encryptPasswordIfNotUsingVariables(trustStorePass)));
		}

		if (!Const.isEmpty(cqlStatement)) {
			retval.append("    ").append(XMLHandler.addTagValue("cql", cqlStatement));
		}

		if (executeForEachInputRow != null) {
			retval.append("    ").append(XMLHandler.addTagValue("execute_for_each_input", executeForEachInputRow));
		}

		if (rowLimit > 0) {
			retval.append("    ").append(XMLHandler.addTagValue("rowLimit", rowLimit));
		}

		retval.append("    ").append(XMLHandler.addTagValue("compression", compression.toString()));

		return retval.toString();
	}

	@Override
	public void loadXML(Node stepnode, List<DatabaseMeta> databases, Map<String, Counter> counters) throws KettleXMLException {
		cassandraNodes = XMLHandler.getTagValue(stepnode, "cassandra_nodes");
		cassandraPort = XMLHandler.getTagValue(stepnode, "cassandra_port");
		username = XMLHandler.getTagValue(stepnode, "username");
		password = XMLHandler.getTagValue(stepnode, "password");
		if (!Const.isEmpty(password)) {
			password = Encr.decryptPasswordOptionallyEncrypted(password);
		}
		keyspace = XMLHandler.getTagValue(stepnode, "cassandra_keyspace");
		withSSL = "Y".equals(XMLHandler.getTagValue(stepnode, "cassandra_withSSL"));
		trustStoreFilePath = XMLHandler.getTagValue(stepnode, "cassandra_trustStoreFilePath");
		trustStorePass = XMLHandler.getTagValue(stepnode, "cassandra_trustStorePass");
		String sCompression = XMLHandler.getTagValue(stepnode, "compression");
		cqlStatement = XMLHandler.getTagValue(stepnode, "cql");
		executeForEachInputRow = "Y".equals(XMLHandler.getTagValue(stepnode, "execute_for_each_input"));
		String sRowLimit = XMLHandler.getTagValue(stepnode, "rowLimit");

		if (!Const.isEmpty(trustStorePass)) {
			trustStorePass = Encr.decryptPasswordOptionallyEncrypted(trustStorePass);
		}

		if (cassandraNodes == null) {
			cassandraNodes = "";
		}
		if (cassandraPort == null) {
			cassandraPort = "";
		}
		if (username == null) {
			username = "";
		}
		if (password == null) {
			password = "";
		}
		if (keyspace == null) {
			keyspace = "";
		}
		if (trustStoreFilePath == null) {
			trustStoreFilePath = "";
		}
		if (trustStorePass == null) {
			trustStorePass = "";
		}
		if (cqlStatement == null) {
			cqlStatement = "";
		}
		if (!Const.isEmpty(sRowLimit)) {
			rowLimit = Integer.parseInt(sRowLimit);
		}
		compression = Const.isEmpty(sCompression) ? ConnectionCompression.SNAPPY : ConnectionCompression.fromString(sCompression);
	}

	@Override
	public void readRep(Repository rep, ObjectId id_step, List<DatabaseMeta> databases, Map<String, Counter> counters)
			throws KettleException {
		cassandraNodes = rep.getStepAttributeString(id_step, 0, "cassandra_nodes");
		cassandraPort = rep.getStepAttributeString(id_step, 0, "cassandra_port");
		username = rep.getStepAttributeString(id_step, 0, "username");
		password = rep.getStepAttributeString(id_step, 0, "password");
		if (!Const.isEmpty(password)) {
			password = Encr.decryptPasswordOptionallyEncrypted(password);
		}
		keyspace = rep.getStepAttributeString(id_step, 0, "cassandra_keyspace");
		withSSL = rep.getStepAttributeBoolean(id_step, "withSSL");
		trustStoreFilePath = rep.getStepAttributeString(id_step, 0, "cassandra_trustStoreFilePath");
		trustStorePass = rep.getStepAttributeString(id_step, 0, "cassandra_trustStorePass");
		String sCompression = rep.getStepAttributeString(id_step, 0, "compression");
		cqlStatement = rep.getStepAttributeString(id_step, 0, "cql");
		executeForEachInputRow = rep.getStepAttributeBoolean(id_step, "execute_for_each_input");
		rowLimit = (int)rep.getStepAttributeInteger(id_step, "rowLimit");

		if (!Const.isEmpty(trustStorePass)) {
			trustStorePass = Encr.decryptPasswordOptionallyEncrypted(trustStorePass);
		}

		if (cassandraNodes == null) {
			cassandraNodes = "";
		}
		if (cassandraPort == null) {
			cassandraPort = "";
		}
		if (username == null) {
			username = "";
		}
		if (password == null) {
			password = "";
		}
		if (keyspace == null) {
			keyspace = "";
		}
		if (trustStoreFilePath == null) {
			trustStoreFilePath = "";
		}
		if (trustStorePass == null) {
			trustStorePass = "";
		}
		if (cqlStatement == null) {
			cqlStatement = "";
		}
		compression = Const.isEmpty(sCompression) ? ConnectionCompression.SNAPPY : ConnectionCompression.fromString(sCompression);
	}

	@Override
	public void saveRep(Repository rep, ObjectId id_transformation, ObjectId id_step) throws KettleException {
		if (!Const.isEmpty(cassandraNodes)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_nodes", cassandraNodes);
		}

		if (!Const.isEmpty(cassandraPort)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_port", cassandraPort);
		}

		if (!Const.isEmpty(username)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "username", username);
		}

		if (!Const.isEmpty(password)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "password", Encr.encryptPasswordIfNotUsingVariables(password));
		}

		if (!Const.isEmpty(keyspace)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_keyspace", keyspace);
		}

		if (withSSL != null) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "withSSL", withSSL);
		}

		if (!Const.isEmpty(trustStoreFilePath)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_trustStoreFilePath", trustStoreFilePath);
		}

		if (!Const.isEmpty(trustStorePass)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cassandra_trustStorePass",
					Encr.encryptPasswordIfNotUsingVariables(trustStorePass));
		}

		if (compression != null) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "compression", compression.toString());
		}

		if (!Const.isEmpty(cqlStatement)) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "cql", cqlStatement);
		}

		if (executeForEachInputRow != null) {
			rep.saveStepAttribute(id_transformation, id_step, 0, "execute_for_each_input", executeForEachInputRow);
		}

		rep.saveStepAttribute(id_transformation, id_step, 0, "rowLimit", rowLimit);
	}

	@Override
	public StepInterface getStep(StepMeta stepMeta, StepDataInterface stepDataInterface, int copyNr, TransMeta transMeta, Trans trans) {
		return new CassandraInput(stepMeta, stepDataInterface, copyNr, transMeta, trans);
	}

	@Override
	public StepDataInterface getStepData() {
		return new CassandraInputData();
	}

	@Override
	public String getDialogClassName() {
		return CassandraInputDialog.class.getName();
	}

	public StepDialogInterface getDialog(Shell shell, StepMetaInterface meta, TransMeta transMeta, String name) {
		return new CassandraInputDialog(shell, meta, transMeta, name);
	}

	@Override
	public void setDefault() {
		cassandraNodes = "localhost";
		cassandraPort = "9042";
		username = "";
		password = "";
		keyspace = "";
		withSSL = false;
		trustStoreFilePath = "";
		trustStorePass = "";
		executeForEachInputRow = Boolean.FALSE;
		cqlStatement = "select <fields> from <column family> where <condition>;";
		compression = ConnectionCompression.SNAPPY;
		rowLimit = 0;
	}

	public void getFields(RowMetaInterface row, String name, RowMetaInterface[] info, StepMeta nextStep, VariableSpace space,
			Repository repository, IMetaStore metaStore) throws KettleStepException {

		String v_nodes = space.environmentSubstitute(cassandraNodes);
		String v_port = space.environmentSubstitute(cassandraPort);
		String v_keyspace = space.environmentSubstitute(keyspace);
		String v_cql = space.environmentSubstitute(cqlStatement);

		// check if enough information is given to create Cassandra connection
		if (Const.isEmpty(v_nodes) || Const.isEmpty(v_port) || Const.isEmpty(v_keyspace) || Const.isEmpty(v_cql)) {
			return;
		}

		String v_user = space.environmentSubstitute(username);
		String v_pass = space.environmentSubstitute(password);
		String v_trustfile = space.environmentSubstitute(trustStoreFilePath);
		String v_trustpass = space.environmentSubstitute(trustStorePass);

		// create connection
		logDebug("meta: opening connection");
		CassandraConnection connection;
		try {
			connection = Utils.connect(v_nodes, v_port, v_user, v_pass, v_keyspace, withSSL, v_trustfile, v_trustpass, compression);
		}
		catch (CassandraConnectionException e) {
			throw new KettleStepException("Failed to create connection", e);
		}
		Session session = connection.getSession();

		// let Cassandra parse statement
		logDebug("meta: parsing cql '" + v_cql + "'");
		ResultSet rs = session.execute(v_cql);
		createOutputRowMeta(row, rs);

		connection.release(); // do not wait for closed connection
	}

	public void createOutputRowMeta(RowMetaInterface row, ResultSet rs) {
		row.clear(); // start afresh - eats the input

		for (ColumnDefinitions.Definition d : rs.getColumnDefinitions()) {
			logDebug(d.getName() + ',' + d.getType().getName() + ',' + d.getType().asJavaClass().getName());

			ValueMeta valueMeta = new ValueMeta(d.getName(), Utils.convertDataType(d.getType()), ValueMetaInterface.STORAGE_TYPE_NORMAL);
			valueMeta.setTrimType(ValueMetaInterface.TRIM_TYPE_NONE);
			row.addValueMeta(valueMeta);
		}
	}

	/**
	 * @return the list of cassadraNodes
	 */
	public String getCassadraNodes() {
		return cassandraNodes;
	}

	/**
	 * @param cassadraNodes
	 *            the list of cassadraNodes to set
	 */
	public void setCassadraNodes(String cassadraNodes) {
		this.cassandraNodes = cassadraNodes;
	}

	/**
	 * @return the cassandraPort
	 */
	public String getCassandraPort() {
		return cassandraPort;
	}

	/**
	 * @param cassandraPort
	 *            the cassandraPort to set
	 */
	public void setCassandraPort(String cassandraPort) {
		this.cassandraPort = cassandraPort;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the CQL
	 */
	public String getCqlStatement() {
		return cqlStatement;
	}

	/**
	 * @param CQL
	 *            the CQL to set
	 */
	public void setCqlStatement(String CQL) {
		cqlStatement = CQL;
	}

	/**
	 * @return the rowLimit
	 */
	public int getRowLimit() {
		return rowLimit;
	}

	/**
	 * @param rowLimit
	 *            the rowLimit to set
	 */
	public void setRowLimit(int rowLimit) {
		this.rowLimit = rowLimit;
	}

	/**
	 * @return the executeForEachInputRow
	 */
	public Boolean getExecuteForEachInputRow() {
		return executeForEachInputRow;
	}

	/**
	 * @param executeForEachInputRow
	 *            the executeEachInputRow to set
	 */
	public void setExecuteForEachInputRow(Boolean executeForEachInputRow) {
		this.executeForEachInputRow = executeForEachInputRow;
	}

	/**
	 * @return the keyspace
	 */
	public String getKeyspace() {
		return keyspace;
	}

	/**
	 * @param keyspace
	 *            the keyspace to set
	 */
	public void setKeyspace(String keyspace) {
		this.keyspace = keyspace;
	}

	/**
	 * @return the withSSL
	 */
	public Boolean getWithSSL() {
		return withSSL;
	}

	/**
	 * @param withSSL
	 *            the withSSL to set
	 */
	public void setWithSSL(Boolean withSSL) {
		this.withSSL = withSSL;
	}

	/**
	 * @return the trustStoreFilePath
	 */
	public String getTrustStoreFilePath() {
		return trustStoreFilePath;
	}

	/**
	 * @param trustStoreFilePath
	 *            the trustStoreFilePath to set
	 */
	public void setTrustStoreFilePath(String trustStoreFilePath) {
		this.trustStoreFilePath = trustStoreFilePath;
	}

	/**
	 * @return the trustStorePass
	 */
	public String getTrustStorePass() {
		return trustStorePass;
	}

	/**
	 * @param trustStorePass
	 *            the trustStorePass to set
	 */
	public void setTrustStorePass(String trustStorePass) {
		this.trustStorePass = trustStorePass;
	}

	public ConnectionCompression getCompression() {
		return compression;
	}

	public void setCompression(ConnectionCompression compression) {
		this.compression = compression;
	}

}
