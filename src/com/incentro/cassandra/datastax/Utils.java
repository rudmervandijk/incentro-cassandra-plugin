/*******************************************************************************
 *
 * Incentro
 *
 * Copyright (C) 2016 by Incentro : https://www.incentro.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/
package com.incentro.cassandra.datastax;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.pentaho.di.core.row.ValueMetaInterface;

import com.datastax.driver.core.DataType;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;

public class Utils {

	private static final LoadingCache<Map<String, String>, CassandraConnection> cache = CacheBuilder.newBuilder()
			.build(new CacheLoader<Map<String, String>, CassandraConnection>() {
				@Override
				public CassandraConnection load(Map<String, String> config) throws Exception {
					return new CassandraConnection(config, cache);
				}
			});

	public static CassandraConnection connect(final String nodes, final String port, final String username, final String password,
			final String keyspace, final boolean withSSL, final String truststoreFilePath, final String truststorePass,
			final ConnectionCompression compression) throws CassandraConnectionException {

		Map<String, String> config = buildConfig(nodes, port, username, password, keyspace, withSSL, truststoreFilePath, truststorePass,
				compression);
		try {
			while (true) {
				// Get (or create) the CassandraConnection from the cache
				CassandraConnection connection = cache.get(config);

				if (connection.acquire()) return connection;

				// If we failed to acquire, it means we raced with the release of the last reference to the connection
				// (which also removed it from the cache).
				// Loop to try again, which will cause the cache to create a new instance.
			}
		}
		catch (ExecutionException e) {
			throw new CassandraConnectionException("Failed to get a connection", e);
		}
	}

	// We use this as a key in our cache: two clients asking for the same configuration at the same time will get back the same object
	private static Map<String, String> buildConfig(final String nodes, final String port, final String username, final String password,
			final String keyspace, final boolean withSSL, final String truststoreFilePath, final String truststorePass,
			final ConnectionCompression compression) {
		ImmutableMap.Builder<String, String> builder = ImmutableMap.<String, String> builder().put("nodes", nodes).put("port", port)
				.put("withSSL", Boolean.toString(withSSL));
		if (username != null) builder.put("username", username);
		if (password != null) builder.put("password", password);
		if (keyspace != null) builder.put("keyspace", keyspace);
		if (truststoreFilePath != null) builder.put("truststoreFilePath", truststoreFilePath);
		if (truststorePass != null) builder.put("truststorePass", truststorePass);
		if (compression != null) builder.put("compression", compression.toString());
		return builder.build();
	}

	/**
	 * convert a Cassandra datatype to Pentaho stream type
	 *
	 * @param type The Cassandra datatype to convert
	 * @return the Pentaho ValueMeta
	 */
	public static int convertDataType(DataType type) {
		switch(type.getName()) {
		case BIGINT:
		case COUNTER:
		case INT:
		case VARINT:
			return ValueMetaInterface.TYPE_INTEGER;
		case ASCII:
		case TEXT:
		case VARCHAR:
			return ValueMetaInterface.TYPE_STRING;
		case BOOLEAN:
			return ValueMetaInterface.TYPE_BOOLEAN;
		case FLOAT:
		case DOUBLE:
			return ValueMetaInterface.TYPE_NUMBER;
		case DECIMAL:
			return ValueMetaInterface.TYPE_BIGNUMBER;
		case TIMESTAMP:
			return ValueMetaInterface.TYPE_DATE;
		default:
			// blob
			return ValueMetaInterface.TYPE_BINARY;
		}
	}
}
