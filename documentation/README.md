Step Help documentation
=======================

This folder contains Help documentation for the plugin steps.
The documentation is written in html so that Spoon can display it.

Each step has it's own help page and is displayed through the
GitHub preview functionality since BitBucket does not allow
raw HTML files.

# Updating documentation

Since it is necessary to link to the exact commit the first step
will always be to commit *only* the change to the documentation.

Now you can determine the correct commit hash and update it in
the documentationUrl annotation of the Meta class(es) for which
the help files were changed.

